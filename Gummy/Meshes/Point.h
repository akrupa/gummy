//
//  point.h
//  Gummy
//
//  Created by Adrian Krupa on 23.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef Gummy_point_h
#define Gummy_point_h

namespace Mesh {
    
    struct Point {
        union indices {
            struct
            {
                unsigned int a;
            };
            unsigned int v[1];
        } indices;
    };
    
}

#endif
