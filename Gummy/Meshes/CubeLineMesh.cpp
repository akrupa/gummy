//
//  CubeLineMesh.cpp
//  Gummy
//
//  Created by Adrian Krupa on 22.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "CubeLineMesh.h"

Mesh::CubeLineMesh::CubeLineMesh() : LineMesh() {
    
    Vertex v;
    v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
    v.color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    
    v.position = glm::vec3(-0.5f, -0.5f, 0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 0.5f, -0.5f, 0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 0.5f, -0.5f, -0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-0.5f, -0.5f, -0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-0.5f, 0.5f, 0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 0.5f, 0.5f, 0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 0.5f, 0.5f, -0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-0.5f, 0.5f, -0.5f);
    vertexes.push_back(v);
    
    
    Line l;
    
    l.indices.a = 0;
    l.indices.b = 1;
    lines.push_back(l);
    
    l.indices.a = 1;
    l.indices.b = 2;
    lines.push_back(l);
    
    l.indices.a = 2;
    l.indices.b = 3;
    lines.push_back(l);
    
    l.indices.a = 3;
    l.indices.b = 0;
    lines.push_back(l);
    
    l.indices.a = 0;
    l.indices.b = 4;
    lines.push_back(l);
    
    l.indices.a = 1;
    l.indices.b = 5;
    lines.push_back(l);
    
    l.indices.a = 2;
    l.indices.b = 6;
    lines.push_back(l);
    
    l.indices.a = 3;
    l.indices.b = 7;
    lines.push_back(l);
    
    l.indices.a = 4;
    l.indices.b = 5;
    lines.push_back(l);
    
    l.indices.a = 5;
    l.indices.b = 6;
    lines.push_back(l);
    
    l.indices.a = 6;
    l.indices.b = 7;
    lines.push_back(l);
    
    l.indices.a = 7;
    l.indices.b = 4;
    lines.push_back(l);
    
    /*
    t.indices.a = 0;
    t.indices.b = 1;
    t.indices.c = 2;
    triangles.push_back(t);
    
    t.indices.a = 0;
    t.indices.b = 2;
    t.indices.c = 3;
    triangles.push_back(t);
    */
    
}
