//
//  PlaneMesh.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__PlaneMesh__
#define __Gummy__PlaneMesh__

#include <stdio.h>
#include "TriangleMesh.h"
#include "Vertex.h"

namespace Mesh {
    
    class PlaneMesh : public TriangleMesh {
        
    public:
        PlaneMesh();
    };
    
}

#endif /* defined(__Gummy__PlaneMesh__) */
