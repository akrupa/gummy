//
//  Vertex.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef Gummy_Vertex_h
#define Gummy_Vertex_h

#include "glm.hpp"

namespace Mesh {
    
    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec4 color;
    };
    
}


#endif
