//
//  PointsMesh.cpp
//  Gummy
//
//  Created by Adrian Krupa on 23.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "PointsMesh.h"

Mesh::PointsMesh::PointsMesh() : Mesh() {
    points = std::vector<Point>();
}

Mesh::PointsMesh::PointsMesh(std::vector<Vertex> points) : Mesh() {
    this->points = std::vector<Point>();
    Point p;
    for(std::size_t i=0; i < points.size(); ++i) {
        p.indices.a = (unsigned int)i;
        this->points.push_back(p);
    }
    this->vertexes = points;

}

unsigned int* Mesh::PointsMesh::GetIndexData() {
    return (unsigned int*)points.data();
}

unsigned int Mesh::PointsMesh::GetIndexDataSize() {
    return (unsigned int)points.size() * sizeof(Point);
}

unsigned int Mesh::PointsMesh::GetIndexDrawSize() {
    return (unsigned int)points.size();
}

Mesh::Vertex* Mesh::PointsMesh::GetVertexData() {
    return (Vertex*)vertexes.data();
}

unsigned int Mesh::PointsMesh::GetVertexDataSize() {
    return (unsigned int)vertexes.size() * sizeof(Vertex);
}

unsigned int Mesh::PointsMesh::GetDrawType() {
    return GL_POINTS;
}