//
//  LineMesh.h
//  Gummy
//
//  Created by Adrian Krupa on 21.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__LineMesh__
#define __Gummy__LineMesh__

#include <stdio.h>
#include "Mesh.h"
#include "Line.h"

namespace Mesh {
    
    class LineMesh : public Mesh {
        
    protected:
        std::vector<Line> lines;
        LineMesh();
        
    public:
        LineMesh(std::vector<Vertex> points, std::vector<Line> lines);

        virtual unsigned int *GetIndexData();
        virtual unsigned int GetIndexDataSize();
        
        virtual Vertex *GetVertexData();
        virtual unsigned int GetVertexDataSize();
        
        virtual unsigned int GetIndexDrawSize();
        
        virtual unsigned int GetDrawType();
    };
}

#endif /* defined(__Gummy__LineMesh__) */
