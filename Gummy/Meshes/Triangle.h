//
//  Triangle.h
//  Gummy
//
//  Created by Adrian Krupa on 22.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef Gummy_Triangle_h
#define Gummy_Triangle_h

namespace Mesh {
    struct Triangle {
        union indices {
            struct
            {
                unsigned int a,b,c;
            };
            unsigned int v[3];
        } indices;
    };
}

#endif
