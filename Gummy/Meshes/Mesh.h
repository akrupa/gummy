//
//  Mesh.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__Mesh__
#define __Gummy__Mesh__

#include <vector>
#include "Vertex.h"
#include <OpenGL/gl3.h>

namespace Mesh {
    class Mesh {
        
    protected:
        std::vector<Vertex> vertexes;
        
    public:
        bool isDirty;
        Mesh();
        virtual unsigned int *GetIndexData() = 0;
        virtual unsigned int GetIndexDataSize() = 0;
        
        virtual Vertex *GetVertexData() = 0;
        virtual unsigned int GetVertexDataSize() = 0;
        
        virtual unsigned int GetIndexDrawSize() = 0;
        
        virtual unsigned int GetDrawType() = 0;
        
        void setVertexes(std::vector<Vertex> vertexes);
        
    };
}

#endif /* defined(__Gummy__Mesh__) */
