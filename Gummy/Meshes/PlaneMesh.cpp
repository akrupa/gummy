//
//  PlaneMesh.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "PlaneMesh.h"

Mesh::PlaneMesh::PlaneMesh() : TriangleMesh() {
    
    Vertex v;
    v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
    v.color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    
    v.position = glm::vec3(-0.5f, 0.0f, 0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 0.5f, 0.0f, 0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3( 0.5f, 0.0f, -0.5f);
    vertexes.push_back(v);
    
    v.position = glm::vec3(-0.5f, 0.0f, -0.5f);
    vertexes.push_back(v);
    
    
    Triangle t;
    
    t.indices.a = 0;
    t.indices.b = 1;
    t.indices.c = 2;
    triangles.push_back(t);
    
    t.indices.a = 0;
    t.indices.b = 2;
    t.indices.c = 3;
    triangles.push_back(t);
    
}
