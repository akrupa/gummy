//
//  TriangleMesh.h
//  Gummy
//
//  Created by Adrian Krupa on 21.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__TriangleMesh__
#define __Gummy__TriangleMesh__

#include <stdio.h>
#include "Mesh.h"
#include "Triangle.h"

namespace Mesh {
    
    class TriangleMesh : public Mesh {
        
    protected:
        std::vector<Triangle> triangles;
        TriangleMesh();
        
    public:
        TriangleMesh(std::vector<Vertex> points, std::vector<Triangle> triangles);

        virtual unsigned int *GetIndexData();
        virtual unsigned int GetIndexDataSize();
        
        virtual Vertex *GetVertexData();
        virtual unsigned int GetVertexDataSize();
        
        virtual unsigned int GetIndexDrawSize();
        virtual unsigned int GetDrawType();
    };
    
}

#endif /* defined(__Gummy__TriangleMesh__) */
