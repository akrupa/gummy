//
//  Mesh.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "Mesh.h"

Mesh::Mesh::Mesh() {
    vertexes = std::vector<Vertex>();
}

void Mesh::Mesh::setVertexes(std::vector<Vertex> vertexes) {
    this->vertexes = vertexes;
    isDirty = true;
}
