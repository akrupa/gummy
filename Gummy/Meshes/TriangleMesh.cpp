//
//  TriangleMesh.cpp
//  Gummy
//
//  Created by Adrian Krupa on 21.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "TriangleMesh.h"

Mesh::TriangleMesh::TriangleMesh() : Mesh() {
    triangles = std::vector<Triangle>();
}

Mesh::TriangleMesh::TriangleMesh(std::vector<Vertex> points, std::vector<Triangle> triangles) {
    this->triangles = triangles;
    this->vertexes = points;
}

unsigned int* Mesh::TriangleMesh::GetIndexData() {
    return (unsigned int*)triangles.data();
}

unsigned int Mesh::TriangleMesh::GetIndexDataSize() {
    return (unsigned int)triangles.size() * sizeof(Triangle);
}

unsigned int Mesh::TriangleMesh::GetIndexDrawSize() {
    return (unsigned int)triangles.size() * 3;
}

Mesh::Vertex* Mesh::TriangleMesh::GetVertexData() {
    return (Vertex*)vertexes.data();
}

unsigned int Mesh::TriangleMesh::GetVertexDataSize() {
    return (unsigned int)vertexes.size() * sizeof(Vertex);
}

unsigned int Mesh::TriangleMesh::GetDrawType() {
    return GL_TRIANGLES;
}