//
//  LineMesh.cpp
//  Gummy
//
//  Created by Adrian Krupa on 21.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "LineMesh.h"

Mesh::LineMesh::LineMesh() : Mesh() {
    lines = std::vector<Line>();
}

Mesh::LineMesh::LineMesh(std::vector<Vertex> points, std::vector<Line> lines) {
    this->lines = lines;
    this->vertexes = points;
}


unsigned int* Mesh::LineMesh::GetIndexData() {
    return (unsigned int*)lines.data();
}

unsigned int Mesh::LineMesh::GetIndexDataSize() {
    return (unsigned int)lines.size() * sizeof(Line);
}

unsigned int Mesh::LineMesh::GetIndexDrawSize() {
    return (unsigned int)lines.size() * 2;
}

Mesh::Vertex* Mesh::LineMesh::GetVertexData() {
    return (Vertex*)vertexes.data();
}

unsigned int Mesh::LineMesh::GetVertexDataSize() {
    return (unsigned int)vertexes.size() * sizeof(Vertex);
}

unsigned int Mesh::LineMesh::GetDrawType() {
    return GL_LINES;
}
