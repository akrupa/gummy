//
//  CubeLineMesh.h
//  Gummy
//
//  Created by Adrian Krupa on 22.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__CubeLineMesh__
#define __Gummy__CubeLineMesh__

#include <stdio.h>
#include "LineMesh.h"
#include "Vertex.h"

namespace Mesh {
    
    class CubeLineMesh : public LineMesh {
        
    public:
        CubeLineMesh();
    };
    
}

#endif /* defined(__Gummy__CubeLineMesh__) */
