//
//  Cube64PointMesh.h
//  Gummy
//
//  Created by Adrian Krupa on 23.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__Cube64PointMesh__
#define __Gummy__Cube64PointMesh__

#include <stdio.h>
#include "PointsMesh.h"
#include "Vertex.h"

namespace Mesh {
    
    class Cube64PointsMesh : public PointsMesh {
        
    public:
        Cube64PointsMesh();
    };
    
}
#endif /* defined(__Gummy__Cube64PointMesh__) */
