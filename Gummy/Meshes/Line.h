//
//  Line.h
//  Gummy
//
//  Created by Adrian Krupa on 22.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef Gummy_Line_h
#define Gummy_Line_h

namespace Mesh {
    
    struct Line {
        union indices {
            struct
            {
                unsigned int a,b;
            };
            unsigned int v[2];
        } indices;
    };
    
}

#endif
