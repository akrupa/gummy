//
//  Cube64PointMesh.cpp
//  Gummy
//
//  Created by Adrian Krupa on 23.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "Cube64PointsMesh.h"

Mesh::Cube64PointsMesh::Cube64PointsMesh() : PointsMesh() {
    
    Vertex v;
    v.normal = glm::vec3(0.0f, 1.0f, 0.0f);
    v.color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
    int counter = 0;
    Point p;
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            for (int k=0; k<4; k++) {
                v.position = glm::vec3(-0.5f+i*1.0f/3.0f, -0.5f+j*1.0f/3.0f, -0.5f+k*1.0f/3.0f);
                p.indices.a = counter;
                
                vertexes.push_back(v);
                points.push_back(p);
                
                counter++;
            }
        }
    }
}