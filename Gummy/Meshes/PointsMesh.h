//
//  PointsMesh.h
//  Gummy
//
//  Created by Adrian Krupa on 23.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__PointsMesh__
#define __Gummy__PointsMesh__

#include <stdio.h>
#include "Mesh.h"
#include "Point.h"

namespace Mesh {
    
    class PointsMesh : public Mesh {
        
    protected:
        std::vector<Point> points;
        PointsMesh();
        
    public:
        PointsMesh(std::vector<Vertex> points);
        virtual unsigned int *GetIndexData();
        virtual unsigned int GetIndexDataSize();
        
        virtual Vertex *GetVertexData();
        virtual unsigned int GetVertexDataSize();
        
        virtual unsigned int GetIndexDrawSize();
        
        virtual unsigned int GetDrawType();
    };
}

#endif /* defined(__Gummy__PointsMesh__) */
