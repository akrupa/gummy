//
//  Buffer.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__Buffer__
#define __Gummy__Buffer__

#include <iostream>
#include <OpenGL/gl3.h>

namespace Buffer {
    class Buffer {
    protected:
        GLuint m_VAO;
        GLuint m_indiciesCount;
        
        GLuint m_indexBuffer;
        GLuint m_vertexBuffer;
        
    public:
        GLuint getVAO();
        GLuint getIndicesCount();
    };
}

#endif /* defined(__Gummy__Buffer__) */
