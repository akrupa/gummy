//
//  Buffer.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "Buffer.h"

GLuint Buffer::Buffer::getVAO()
{
    return m_VAO;
}

GLuint Buffer::Buffer::getIndicesCount()
{
    return m_indiciesCount;
}