//
//  MeshBuffer.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__MeshBuffer__
#define __Gummy__MeshBuffer__

#include <iostream>
#include "Buffer.h"
#include "Vertex.h"

namespace Buffer {
    class MeshBuffer : public Buffer {
        
        
    public:
        void setup();
        void fillVertexData(Mesh::Vertex *data, size_t size, size_t offset);
        void fillIndexData(GLuint *data, size_t size, size_t offset);
    };
}
#endif /* defined(__Gummy__MeshBuffer__) */
