//
//  AKOpenGLView.m
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKOpenGLView.h"

@interface AKOpenGLView() {
    BOOL _isAltPressed;
    BOOL _isSpacePressed;
}
@end

@implementation AKOpenGLView

- (BOOL) acceptsFirstResponder {
    return YES;
}

- (void) awakeFromNib {
    NSOpenGLPixelFormatAttribute attrs[] =
    {
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFADepthSize, 24,
        // Must specify the 3.2 Core Profile to use OpenGL 3.2
        NSOpenGLPFAOpenGLProfile,
        NSOpenGLProfileVersion3_2Core,
        NSOpenGLPFAAccelerated,
        NSOpenGLPFAMultisample,
        NSOpenGLPFASampleBuffers, (NSOpenGLPixelFormatAttribute)2,
        NSOpenGLPFASamples, (NSOpenGLPixelFormatAttribute)16,
        0
    };
    
    NSOpenGLPixelFormat *pf = [[NSOpenGLPixelFormat alloc] initWithAttributes:attrs];
    
    if (!pf)
    {
        NSLog(@"No OpenGL pixel format");
    }
    
    NSOpenGLContext* context = [[NSOpenGLContext alloc] initWithFormat:pf shareContext:nil];
    
    CGLEnable(static_cast<CGLContextObj>([context CGLContextObj]), kCGLCECrashOnRemovedFunctions);
    
    [self setPixelFormat:pf];
    [self setOpenGLContext:context];
    [self setWantsBestResolutionOpenGLSurface:YES];
    
    pressedKeys = [[NSMutableDictionary alloc]init];
    
    NSLog(@"%s",  glGetString(GL_VERSION));
}


- (void) prepareOpenGL {
    [super prepareOpenGL];
    [self initGL];
}

- (void) initGL {
    [[self openGLContext] makeCurrentContext];
    
    GLint swapInt = 1;
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    // Create a display link capable of being used with all active displays
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    
    // Set the renderer output callback function
    CVDisplayLinkSetOutputCallback(displayLink, &MyDisplayLinkCallback, (__bridge void *)(self));
    
    // Set the display link for the current renderer
    CGLContextObj cglContext = [[self openGLContext] CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = [[self pixelFormat] CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink, cglContext, cglPixelFormat);
    
    // Activate the display link
    CVDisplayLinkStart(displayLink);
}

static CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp* now, const CVTimeStamp* outputTime, CVOptionFlags flagsIn, CVOptionFlags* flagsOut, void* displayLinkContext)
{
    CVReturn result = [(__bridge AKOpenGLView*)displayLinkContext getFrameForTime:outputTime];
    return result;
}

- (CVReturn) getFrameForTime:(const CVTimeStamp*)outputTime
{
    //[self drawView];
    //NSLog(@"%lld",  outputTime->videoRefreshPeriod);
    [self updateKeyboardMove];
    [self setNeedsDisplay:TRUE];
    return kCVReturnSuccess;
}

- (void) updateKeyboardMove {
    float speed = 0.3;
    float x = 0;
    float y = 0;
    float z = 0;
    if ([pressedKeys objectForKey:@"w"] != nil) {
        z += speed;
    }
    if ([pressedKeys objectForKey:@"s"] != nil) {
        z -= speed;
    }
    if ([pressedKeys objectForKey:@"a"] != nil) {
        x += speed;
    }
    if ([pressedKeys objectForKey:@"d"] != nil) {
        x -= speed;
    }
    if ([pressedKeys objectForKey:@"q"] != nil) {
        y += speed;
    }
    if ([pressedKeys objectForKey:@"e"] != nil) {
        y -= speed;
    }
    
    if(cameraController != nil) {
        cameraController -> keyboardMove(x, y, z);
    }
}

- (void) drawRect: (NSRect) bounds {
    
    [self drawView];
}

- (void) drawView {
    [[self openGLContext] makeCurrentContext];
    CGLLockContext(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));
    float aspect = self.frame.size.width/self.frame.size.height;
    //glViewport(0.0f, 0.0f, self.frame.size.width, self.frame.size.height/2);
    if(self.updateHandler) {
        self.updateHandler();
    }
    cameraController->setAspectRatio(aspect);
    renderer->renderWithCameraController(cameraController);
    
    CGLFlushDrawable(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));
    CGLUnlockContext(static_cast<CGLContextObj>([[self openGLContext] CGLContextObj]));
}

- (void) setCameraController:(Controller::CameraController*)cameraControllerToSave {
    self->cameraController = cameraControllerToSave;
}

- (void) setRenderer:(Renderer::Renderer*)rendererToSave {
    self->renderer = rendererToSave;
    self->renderer->setup();
    self->renderer->initScene();
}

- (void) setHandlerForUpdate:(void (^)())updateHandler {
    self.updateHandler = updateHandler;
}

- (void) setHandlerForBoxMove:(void (^)(float deltaX, float deltaY))boxMoveHandler {
    self.moveBoxHandler = boxMoveHandler;
}

- (void) setHandlerForBoxRotate:(void (^)(float deltaX, float deltaY))boxRotateHandler {
    self.rotateBoxHandler = boxRotateHandler;
}

- (void) renewGState {
    [[self window] disableScreenUpdatesUntilFlush];
}

- (void) mouseDragged:(NSEvent *)theEvent {
    if (_isAltPressed) {
        //cameraController->rotateAroundPoint(glm::vec3(0,0,0), [theEvent deltaX]/1000, [theEvent deltaY]/1000);
        if (self.rotateBoxHandler) {
            self.rotateBoxHandler(10*[theEvent deltaX]/self.frame.size.width, 10*[theEvent deltaY]/self.frame.size.height);
        }
    } else if (_isSpacePressed) {
        if (self.moveBoxHandler) {
            self.moveBoxHandler(2*[theEvent deltaX]/self.frame.size.width, -2*[theEvent deltaY]/self.frame.size.height);
        }
    } else {
        cameraController->mouseDrag([theEvent deltaX], [theEvent deltaY]);
    }
}

- (void) keyDown:(NSEvent *)theEvent {

    switch( [theEvent keyCode] ) {
        case KEY_W:
            [pressedKeys removeObjectForKey:@"W"];
            [pressedKeys setObject:[NSNull null] forKey:@"w"];
            break;
        case KEY_S:
            [pressedKeys removeObjectForKey:@"S"];
            [pressedKeys setObject:[NSNull null] forKey:@"s"];
            break;
        case KEY_A:
            [pressedKeys removeObjectForKey:@"A"];
            [pressedKeys setObject:[NSNull null] forKey:@"a"];
            break;
        case KEY_D:
            [pressedKeys removeObjectForKey:@"D"];
            [pressedKeys setObject:[NSNull null] forKey:@"d"];
            break;
        case KEY_Q:
            [pressedKeys removeObjectForKey:@"Q"];
            [pressedKeys setObject:[NSNull null] forKey:@"q"];
            break;
        case KEY_E:
            [pressedKeys removeObjectForKey:@"E"];
            [pressedKeys setObject:[NSNull null] forKey:@"e"];
            break;
        case KEY_SPACE:
            _isSpacePressed = true;
            break;
    }
}

- (void) setFirstResponder {
    [self->_window makeFirstResponder:self];
}

- (void)flagsChanged:(NSEvent *)theEvent {
    if ([theEvent modifierFlags] & NSAlternateKeyMask) {
        _isAltPressed = YES;
    } else if(!([theEvent modifierFlags] & NSAlternateKeyMask)) {
        _isAltPressed = NO;
    }
}

- (void) keyUp:(NSEvent *)theEvent {
    
    switch( [theEvent keyCode] ) {
        case KEY_W:
            [pressedKeys removeObjectForKey:@"W"];
            [pressedKeys removeObjectForKey:@"w"];
            break;
        case KEY_S:
            [pressedKeys removeObjectForKey:@"S"];
            [pressedKeys removeObjectForKey:@"s"];
            break;
        case KEY_A:
            [pressedKeys removeObjectForKey:@"A"];
            [pressedKeys removeObjectForKey:@"a"];
            break;
        case KEY_D:
            [pressedKeys removeObjectForKey:@"D"];
            [pressedKeys removeObjectForKey:@"d"];
            break;
        case KEY_Q:
            [pressedKeys removeObjectForKey:@"Q"];
            [pressedKeys removeObjectForKey:@"q"];
            break;
        case KEY_E:
            [pressedKeys removeObjectForKey:@"E"];
            [pressedKeys removeObjectForKey:@"e"];
            break;
        case KEY_SPACE:
            _isSpacePressed = false;
            break;
    }
}


@end
