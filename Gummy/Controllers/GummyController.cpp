//
//  GummyController.cpp
//  Gummy
//
//  Created by Adrian Krupa on 23.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include <thread>
#include "GummyController.h"
#include "DiffuseShader.h"
#include "AmbientShader.h"
#include "matrix_transform.hpp"
#include "tiny_obj_loader.h"
#include "rotate_vector.hpp"

using namespace glm;

#pragma mark Initialize
void Controller::GummyController::init() {
    
    bernsteinMatrix[0][0] = 1;
    bernsteinMatrix[1][1] = 3;
    bernsteinMatrix[2][2] = 3;
    bernsteinMatrix[3][3] = 1;
    bernsteinMatrix[2][1] = -6;
    bernsteinMatrix[1][0] = bernsteinMatrix[3][2] = -3;
    bernsteinMatrix[2][0] = bernsteinMatrix[3][1] = 3;
    bernsteinMatrix[3][0] = -1;
    
    initVectors();
    initSimulationParameters();
    initFrameLinesModel();
    initControlPointsModel();
    initSpringsLinesModel();
    initBoundingBoxModel();
    initBezierCubeModel();
    initBezierSphereModel();
    initGummyModel();
    
    
    //controlPointsLastPositions[8] += vec3(1,0.01,0);
    //controlPointsLastPositions[16] -= vec3(1,0.01,0);
    //controlPointsLastPositions[22] += vec3(0,-1,1);
    
    isInited = true;
}

void Controller::GummyController::initVectors() {
    controlPoints = std::vector<Mesh::Vertex>();
    springLines = std::vector<Mesh::Line>();
    pointsForces = std::vector<glm::vec3>();
    controlPointsLastPositions = std::vector<glm::vec3>();
    springsBaseLength = std::vector<float>();
    framePoints = std::vector<Mesh::Vertex>();
    frameLines = std::vector<Mesh::Line>();
    boundingBoxPoints = std::vector<Mesh::Vertex>();
    boundingBoxTriangles = std::vector<Mesh::Triangle>();
    bezierCubePoints = std::vector<Mesh::Vertex>();
    bezierCubeTriangles = std::vector<Mesh::Triangle>();
    bezierSpherePoints = std::vector<Mesh::Vertex>();
    bezierSphereTriangles = std::vector<Mesh::Triangle>();
    bezierSphereParametrizationPoints = std::vector<glm::vec3>();
    gummyPoints = std::vector<Mesh::Vertex>();
    gummyTriangles = std::vector<Mesh::Triangle>();
    gummyParametrizationPoints = std::vector<glm::vec3>();
    
}

void Controller::GummyController::initSimulationParameters() {
    this->s = 0;
    this->c1 = 10;
    this->c2 = 50;
    this->k = 1;
    this->l0 = 1.0f/3.0f;
    this->delta = 1.0f/120.0f;
    this->m = 0.2;
    this->u = 1;
    this->boundingBoxSize = glm::vec3(3,2,2);
    this->bezierMeshDensity = 15;
}

void Controller::GummyController::initFrameLinesModel() {
    Mesh::Vertex v;
    Mesh::Line l;
    v.normal = vec3(0.0f, 1.0f, 0.0f);
    v.color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    
    Shader::Shader *shader = new Shader::AmbientShader();
    shader->setup();
    
    for (int i=0; i<8; i++) {
        framePoints.push_back(v);
    }
    l.indices.a = 0;
    l.indices.b = 1;
    frameLines.push_back(l);
    l.indices.a = 1;
    l.indices.b = 2;
    frameLines.push_back(l);
    l.indices.a = 2;
    l.indices.b = 3;
    frameLines.push_back(l);
    l.indices.a = 3;
    l.indices.b = 0;
    frameLines.push_back(l);
    
    l.indices.a = 4;
    l.indices.b = 5;
    frameLines.push_back(l);
    l.indices.a = 5;
    l.indices.b = 6;
    frameLines.push_back(l);
    l.indices.a = 6;
    l.indices.b = 7;
    frameLines.push_back(l);
    l.indices.a = 7;
    l.indices.b = 4;
    frameLines.push_back(l);
    
    l.indices.a = 0;
    l.indices.b = 4;
    frameLines.push_back(l);
    l.indices.a = 1;
    l.indices.b = 5;
    frameLines.push_back(l);
    l.indices.a = 2;
    l.indices.b = 6;
    frameLines.push_back(l);
    l.indices.a = 3;
    l.indices.b = 7;
    frameLines.push_back(l);
    
    frameLinessMesh = new Mesh::LineMesh(framePoints, frameLines);
    frameLineModel = new Model::Model(frameLinessMesh, shader);
    setFramePointsPositions(glm::vec3(0), glm::vec3(0));
}

void Controller::GummyController::initControlPointsModel() {
    Mesh::Vertex v;
    v.normal = vec3(0.0f, 1.0f, 0.0f);
    v.color = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    
    Shader::Shader *shader = new Shader::AmbientShader();
    shader->setup();
    
    glm::vec3 force = glm::vec3(0,0,0);
    
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            for (int k=0; k<4; k++) {
                v.position = vec3(-0.5f+i*1.0f/3.0f, -0.5f+j*1.0f/3.0f, -0.5f+k*1.0f/3.0f);
                controlPoints.push_back(v);
                controlPointsLastPositions.push_back(v.position);
                pointsForces.push_back(force);
            }
        }
    }
    
    
    for (int i=0; i<8; i++) {
        v.position = framePoints[i].position;
        controlPoints.push_back(v);
        if (i==0) {
            //lFrame0 = glm::length(controlPoints[0].position-controlPoints[64].position);
            lFrame0 = 0;
        }
        controlPointsLastPositions.push_back(v.position);
        pointsForces.push_back(force);
    }
    
    controlPointsMesh = new Mesh::PointsMesh(controlPoints);
    controlPointsModel = new Model::Model(controlPointsMesh, shader);
    
}

void Controller::GummyController::initSpringsLinesModel() {
    Mesh::Line l;
    
    Shader::Shader *shader = new Shader::AmbientShader();
    shader->setup();
    
    
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            for (int k=0; k<4; k++) {
                l.indices.a = i*4*4+j*4+k;
                if (k<3) {
                    l.indices.b = i*4*4+j*4+(k+1);
                    springLines.push_back(l);
                    springsBaseLength.push_back(l0);
                }
                if (j<3) {
                    l.indices.b = i*4*4+(j+1)*4+k;
                    springLines.push_back(l);
                    springsBaseLength.push_back(l0);
                }
                
                
                if (k<3 && j<3) {
                    l.indices.b = i*4*4+(j+1)*4+(k+1);
                    springLines.push_back(l);
                    springsBaseLength.push_back(sqrtf(2.0f)*l0);
                }
                
                if (k<3 && j>0) {
                    l.indices.b = i*4*4+(j-1)*4+(k+1);
                    springLines.push_back(l);
                    springsBaseLength.push_back(sqrtf(2.0f)*l0);
                }
                
                
                if (i < 3) {
                    
                    l.indices.b = (i+1)*4*4+j*4+k;
                    springLines.push_back(l);
                    springsBaseLength.push_back(l0);
                    
                    if (k<3) {
                        l.indices.b = (i+1)*4*4+j*4+(k+1);
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(2.0f)*l0);
                    }
                    
                    
                    if (k>0) {
                        l.indices.b = (i+1)*4*4+j*4+(k-1);
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(2.0f)*l0);
                    }
                    
                    
                    
                    if (j<3) {
                        l.indices.b = (i+1)*4*4+(j+1)*4+k;
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(2.0f)*l0);
                    }
                    if (j>0) {
                        l.indices.b = (i+1)*4*4+(j-1)*4+k;
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(2.0f)*l0);
                    }
                    
                    
                    if (k<3 && j<3) {
                        l.indices.b = (i+1)*4*4+(j+1)*4+(k+1);
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(3.0f)*l0);
                    }
                    
                    if (k<3 && j>0) {
                        l.indices.b = (i+1)*4*4+(j-1)*4+(k+1);
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(3.0f)*l0);
                    }
                    
                    if (k>0 && j<3) {
                        l.indices.b = (i+1)*4*4+(j+1)*4+(k-1);
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(3.0f)*l0);
                    }
                    
                    if (k>0 && j>0) {
                        l.indices.b = (i+1)*4*4+(j-1)*4+(k-1);
                        springLines.push_back(l);
                        springsBaseLength.push_back(sqrtf(3.0f)*l0);
                    }
                    
                }
                
            }
        }
    }
    
    
    for (int i=0; i<8; i++) {
        l.indices.a = 64+i;
        switch (i) {
            case 0:
                l.indices.b = 0;
                break;
            case 1:
                l.indices.b = 3;
                break;
            case 2:
                l.indices.b = 15;
                break;
            case 3:
                l.indices.b = 12;
                break;
            case 4:
                l.indices.b = 48;
                break;
            case 5:
                l.indices.b = 51;
                break;
            case 6:
                l.indices.b = 63;
                break;
            case 7:
                l.indices.b = 60;
                break;
        }
        springLines.push_back(l);
        springsBaseLength.push_back(lFrame0);
    }
    
    springLinessMesh = new Mesh::LineMesh(controlPoints, springLines);
    springLinesModel = new Model::Model(springLinessMesh, shader);
}

void Controller::GummyController::initBoundingBoxModel() {
    Shader::Shader *boxShader = new Shader::DiffuseShader();
    boxShader->setup();
    
    Mesh::Vertex boundingBoxVertex;
    Mesh::Triangle boundingBoxTriangle;
    boundingBoxVertex.color = glm::vec4(1,1,1,0.5);
    
    boundingBoxVertex.normal = glm::vec3(1,0,0);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, -boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, -boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    
    boundingBoxVertex.normal = glm::vec3(-1,0,0);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, -boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, -boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    
    boundingBoxVertex.normal = glm::vec3(0,0,1);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, -boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, -boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    
    boundingBoxVertex.normal = glm::vec3(0,0,-1);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, -boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, -boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    
    
    
    boundingBoxVertex.normal = glm::vec3(0,1,0);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, -boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, -boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, -boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, -boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    
    
    
    boundingBoxVertex.normal = glm::vec3(0,-1,0);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(-boundingBoxSize.x, boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, boundingBoxSize.y, boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    boundingBoxVertex.position = glm::vec3(boundingBoxSize.x, boundingBoxSize.y, -boundingBoxSize.z);
    boundingBoxPoints.push_back(boundingBoxVertex);
    
    boundingBoxTriangle.indices.a = 2;
    boundingBoxTriangle.indices.b = 1;
    boundingBoxTriangle.indices.c = 0;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 3;
    boundingBoxTriangle.indices.b = 2;
    boundingBoxTriangle.indices.c = 0;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 4;
    boundingBoxTriangle.indices.b = 5;
    boundingBoxTriangle.indices.c = 6;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 4;
    boundingBoxTriangle.indices.b = 6;
    boundingBoxTriangle.indices.c = 7;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 10;
    boundingBoxTriangle.indices.b = 9;
    boundingBoxTriangle.indices.c = 8;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 11;
    boundingBoxTriangle.indices.b = 10;
    boundingBoxTriangle.indices.c = 8;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 12;
    boundingBoxTriangle.indices.b = 13;
    boundingBoxTriangle.indices.c = 14;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 12;
    boundingBoxTriangle.indices.b = 14;
    boundingBoxTriangle.indices.c = 15;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 16;
    boundingBoxTriangle.indices.b = 17;
    boundingBoxTriangle.indices.c = 18;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 16;
    boundingBoxTriangle.indices.b = 18;
    boundingBoxTriangle.indices.c = 19;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 22;
    boundingBoxTriangle.indices.b = 21;
    boundingBoxTriangle.indices.c = 20;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxTriangle.indices.a = 23;
    boundingBoxTriangle.indices.b = 22;
    boundingBoxTriangle.indices.c = 20;
    boundingBoxTriangles.push_back(boundingBoxTriangle);
    
    boundingBoxMesh = new Mesh::TriangleMesh(boundingBoxPoints, boundingBoxTriangles);
    boundingBoxModel = new Model::Model(boundingBoxMesh, boxShader);
    
}

void Controller::GummyController::initBezierCubeModel() {
    Shader::Shader *bezierCubeShader = new Shader::DiffuseShader();
    bezierCubeShader->setup();
    
    Mesh::Vertex bezierCubeVertex;
    Mesh::Triangle bezierCubeTriangle;
    bezierCubeVertex.color = glm::vec4(1,1,1,1);
    glm::vec3 v = glm::vec3(0);
    bezierCubeVertex.position = v;
    
    int offset = 0;
    bezierCubeVertex.normal = glm::vec3(-1,0,0);
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints.push_back(bezierCubeVertex);
            if (i<bezierMeshDensity && j< bezierMeshDensity) {
                bezierCubeTriangle.indices.a = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j+1+offset;
                bezierCubeTriangle.indices.c = (i+1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
            
            if (i>0 && j>0) {
                bezierCubeTriangle.indices.a = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j-1+offset;
                bezierCubeTriangle.indices.c = (i-1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
        }
    }
    
    offset+=(bezierMeshDensity+1)*(bezierMeshDensity+1);
    bezierCubeVertex.normal = glm::vec3(1,0,0);
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints.push_back(bezierCubeVertex);
            if (i<bezierMeshDensity && j< bezierMeshDensity) {
                bezierCubeTriangle.indices.c = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j+1+offset;
                bezierCubeTriangle.indices.a = (i+1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
            
            if (i>0 && j>0) {
                bezierCubeTriangle.indices.c = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j-1+offset;
                bezierCubeTriangle.indices.a = (i-1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
        }
    }
    
    offset+=(bezierMeshDensity+1)*(bezierMeshDensity+1);
    bezierCubeVertex.normal = glm::vec3(0,-1,0);
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints.push_back(bezierCubeVertex);
            if (i<bezierMeshDensity && j< bezierMeshDensity) {
                bezierCubeTriangle.indices.c = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j+1+offset;
                bezierCubeTriangle.indices.a = (i+1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
            
            if (i>0 && j>0) {
                bezierCubeTriangle.indices.c = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j-1+offset;
                bezierCubeTriangle.indices.a = (i-1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
        }
    }
    
    offset+=(bezierMeshDensity+1)*(bezierMeshDensity+1);
    bezierCubeVertex.normal = glm::vec3(0,1,0);
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints.push_back(bezierCubeVertex);
            if (i<bezierMeshDensity && j< bezierMeshDensity) {
                bezierCubeTriangle.indices.a= i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j+1+offset;
                bezierCubeTriangle.indices.c = (i+1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
            
            if (i>0 && j>0) {
                bezierCubeTriangle.indices.a = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j-1+offset;
                bezierCubeTriangle.indices.c = (i-1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
        }
    }
    
    offset+=(bezierMeshDensity+1)*(bezierMeshDensity+1);
    bezierCubeVertex.normal = glm::vec3(0,0,1);
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints.push_back(bezierCubeVertex);
            if (i<bezierMeshDensity && j< bezierMeshDensity) {
                bezierCubeTriangle.indices.c = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j+1+offset;
                bezierCubeTriangle.indices.a = (i+1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
            
            if (i>0 && j>0) {
                bezierCubeTriangle.indices.c = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j-1+offset;
                bezierCubeTriangle.indices.a = (i-1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
        }
    }
    
    offset+=(bezierMeshDensity+1)*(bezierMeshDensity+1);
    bezierCubeVertex.normal = glm::vec3(0,0,-1);
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints.push_back(bezierCubeVertex);
            if (i<bezierMeshDensity && j< bezierMeshDensity) {
                bezierCubeTriangle.indices.a= i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j+1+offset;
                bezierCubeTriangle.indices.c = (i+1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
            
            if (i>0 && j>0) {
                bezierCubeTriangle.indices.a = i*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangle.indices.b = i*(bezierMeshDensity+1)+j-1+offset;
                bezierCubeTriangle.indices.c = (i-1)*(bezierMeshDensity+1)+j+offset;
                bezierCubeTriangles.push_back(bezierCubeTriangle);
            }
        }
    }
    
    
    
    bezierCubeMesh = new Mesh::TriangleMesh(bezierCubePoints, bezierCubeTriangles);
    bezierCubeModel = new Model::Model(bezierCubeMesh, bezierCubeShader);
    updateBezierCubeModel();
}

void Controller::GummyController::initBezierSphereModel() {
    
    Shader::Shader *bezierSphereShader = new Shader::DiffuseShader();
    bezierSphereShader->setup();
    
    float beta = 0;
    float alpha = 0;
    
    Mesh::Vertex bezierSphereVertex;
    Mesh::Triangle bezierSphereTriangle;
    
    bezierSphereVertex.color = glm::vec4(1,1,1,1);
    
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierSpherePoints.push_back(bezierSphereVertex);
            bezierSphereParametrizationPoints.push_back((glm::vec3(glm::cos(alpha)*glm::sin(beta),
                                                                   glm::cos(beta),
                                                                   glm::sin(alpha)*glm::sin(beta))+glm::vec3(1,1,1))/2.0f);
            alpha += 2*M_PI/(bezierMeshDensity+1);
        }
        beta += M_PI/(bezierMeshDensity);
    }
    
    
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<bezierMeshDensity; j++) {
            
            if (i>0 && i<bezierMeshDensity && j<bezierMeshDensity) {
                bezierSphereTriangle.indices.a = i*(bezierMeshDensity+1)+j;
                bezierSphereTriangle.indices.b = i*(bezierMeshDensity+1)+j+1;
                bezierSphereTriangle.indices.c = (i+1)*(bezierMeshDensity+1)+j;
                bezierSphereTriangles.push_back(bezierSphereTriangle);
            }
            
            if (i>0 && j>0) {
                bezierSphereTriangle.indices.a = i*(bezierMeshDensity+1)+j;
                bezierSphereTriangle.indices.b = i*(bezierMeshDensity+1)+j-1;
                bezierSphereTriangle.indices.c = (i-1)*(bezierMeshDensity+1)+j;
                bezierSphereTriangles.push_back(bezierSphereTriangle);
            }
            
            if (j==bezierMeshDensity-1) {
                if (i>0 && i<bezierMeshDensity) {
                    bezierSphereTriangle.indices.a = i*(bezierMeshDensity+1)+j+1;
                    bezierSphereTriangle.indices.b = i*(bezierMeshDensity+1);
                    bezierSphereTriangle.indices.c = (i+1)*(bezierMeshDensity+1)+j+1;
                    bezierSphereTriangles.push_back(bezierSphereTriangle);
                }
                
                if (i>0) {
                    bezierSphereTriangle.indices.a = i*(bezierMeshDensity+1)+j+1;
                    bezierSphereTriangle.indices.b = i*(bezierMeshDensity+1)+j;
                    bezierSphereTriangle.indices.c = (i-1)*(bezierMeshDensity+1)+j+1;
                    bezierSphereTriangles.push_back(bezierSphereTriangle);
                }
            }
            
            if (j==0 && i>0) {
                bezierSphereTriangle.indices.a = i*(bezierMeshDensity+1);
                bezierSphereTriangle.indices.b = i*(bezierMeshDensity+1)+bezierMeshDensity;
                bezierSphereTriangle.indices.c = (i-1)*(bezierMeshDensity+1);
                bezierSphereTriangles.push_back(bezierSphereTriangle);
            }
            
            /*
             if (j==bezierMeshDensity-1) {
             bezierSphereTriangle.indices.a = i*(bezierMeshDensity+1)+j;
             bezierSphereTriangle.indices.b = i*(bezierMeshDensity+1);
             bezierSphereTriangle.indices.c = (i+1)*(bezierMeshDensity+1)+j;
             bezierSphereTriangles.push_back(bezierSphereTriangle);
             }
             */
        }
        
    }
    
    bezierSphereMesh = new Mesh::TriangleMesh(bezierSpherePoints, bezierSphereTriangles);
    bezierSphereModel = new Model::Model(bezierSphereMesh, bezierSphereShader);
    updateBezierSphereModel();
    
    bezierSphereModel->disableModel();
}

void Controller::GummyController::initGummyModel() {
    std::string inputfile = "gummy.obj";
#if defined(__APPLE__) && !defined(DEBUG)
    inputfile = "../../../" + inputfile;
#endif
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    
    std::string err = tinyobj::LoadObj(shapes, materials, inputfile.c_str());
    
    if (!err.empty()) {
        std::cerr << err << std::endl;
        exit(1);
    }
    
    Shader::Shader *gummyShader = new Shader::DiffuseShader();
    gummyShader->setup();
    
    Mesh::Vertex bezierCubeVertex;
    Mesh::Triangle bezierCubeTriangle;
    bezierCubeVertex.color = glm::vec4(0.5,0.5,1,1);
    glm::vec3 v = glm::vec3(0);
    bezierCubeVertex.position = v;
    
    for (int i=0; i<shapes[0].mesh.positions.size(); i+=3) {
        glm::vec3 parPos = glm::vec3(shapes[0].mesh.positions[i+0], shapes[0].mesh.positions[i+1], shapes[0].mesh.positions[i+2]);
        parPos -= glm::vec3(0,50,0);
        parPos = (parPos + 123.0f)/(123.0f*2.0f);
        gummyParametrizationPoints.push_back(parPos);
        gummyPoints.push_back(bezierCubeVertex);
    }
    for (int i=0; i<shapes[0].mesh.indices.size(); i+=3) {
        bezierCubeTriangle.indices.a = shapes[0].mesh.indices[i+0];
        bezierCubeTriangle.indices.b = shapes[0].mesh.indices[i+1];
        bezierCubeTriangle.indices.c = shapes[0].mesh.indices[i+2];
        gummyTriangles.push_back(bezierCubeTriangle);
    }
    
    gummyMesh = new Mesh::TriangleMesh(gummyPoints, gummyTriangles);
    gummyModel = new Model::Model(gummyMesh, gummyShader);
    updateGummyModel();
    gummyModel->disableModel();
}

#pragma mark -
#pragma mark Update
void Controller::GummyController::update() {
    
    for (int updateCounter = 0; updateCounter < (1.0f/60.0f)*(1.0f/delta); updateCounter++) {
        //setFramePointsPositions(glm::vec3(sinf(s), 0, cosf(s)-1));
        
        for (auto &force : pointsForces) {
            force.x = 0;
            force.y = 0;
            force.z = 0;
        }
        glm::vec3 force = glm::vec3(0,0,0);
        int counter = 0;
        for (auto &line : springLines) {
            glm::vec3 P1 = controlPoints[line.indices.a].position;
            glm::vec3 P2 = controlPoints[line.indices.b].position;
            
            glm::vec3 lastP1 = controlPointsLastPositions[line.indices.a];
            glm::vec3 lastP2 = controlPointsLastPositions[line.indices.b];
            
            float lPlusDelta = glm::length(P1-P2)-springsBaseLength[counter];
            float lt = (glm::length(P1-P2)-glm::length(lastP1-lastP2))/delta;
            
            if (P1 != P2) {
                if (line.indices.a >= 64 || line.indices.b >= 64) {
                    force = -c2*lPlusDelta*glm::normalize(P1-P2) - k*lt*glm::normalize(P1-P2);
                } else {
                    force = -c1*lPlusDelta*glm::normalize(P1-P2) - k*lt*glm::normalize(P1-P2);
                }
            } else {
                force = glm::vec3(0,0,0);
            }
            
            //l = lPlusDelta;
            pointsForces[line.indices.a] += force;
            pointsForces[line.indices.b] -= force;
            counter++;
        }
        
        for(int i=0; i<64; i++) {
            //printf("point %d force: (%f, %f, %f)\n", i, pointsForces[i].x, pointsForces[i].y, pointsForces[i].z);
            glm::vec3 lastPosition = controlPoints[i].position;
            glm::vec3 newPosition = pointsForces[i]*delta*delta/m + 2.0f*controlPoints[i].position - controlPointsLastPositions[i];
            bool isChanged;
            int count;
            do {
                count++;
                isChanged = false;
                if (newPosition.x < -boundingBoxSize.x) {
                    if (u==0) {
                        newPosition.x = -boundingBoxSize.x;
                        lastPosition.x = -boundingBoxSize.x;
                    } else {
                        newPosition.x = (-boundingBoxSize.x-newPosition.x)/u-boundingBoxSize.x;
                        lastPosition.x = (-boundingBoxSize.x-lastPosition.x)/u-boundingBoxSize.x;
                        //newPosition.x = -2*boundingBoxSize.x-newPosition.x;
                        //lastPosition.x = -2*boundingBoxSize.x-lastPosition.x;
                    }
                    isChanged = true;
                    continue;
                }
                if (newPosition.x > boundingBoxSize.x) {
                    if (u==0) {
                        newPosition.x = boundingBoxSize.x;
                        lastPosition.x = boundingBoxSize.x;
                    } else {
                        newPosition.x = (boundingBoxSize.x-newPosition.x)/u+boundingBoxSize.x;
                        lastPosition.x = (boundingBoxSize.x-lastPosition.x)/u+boundingBoxSize.x;
                        //newPosition.x = 2*boundingBoxSize.x-newPosition.x;
                        //lastPosition.x = 2*boundingBoxSize.x-lastPosition.x;
                    }
                    isChanged = true;
                    continue;
                }
                if (newPosition.y < -boundingBoxSize.y) {
                    if (u==0) {
                        newPosition.y = -boundingBoxSize.y;
                        lastPosition.y = -boundingBoxSize.y;
                    } else {
                        newPosition.y = (-boundingBoxSize.y-newPosition.y)/u-boundingBoxSize.y;
                        lastPosition.y = (-boundingBoxSize.y-lastPosition.y)/u-boundingBoxSize.y;
                        //newPosition.y = -2*boundingBoxSize.y-newPosition.y;
                        //lastPosition.y = -2*boundingBoxSize.y-lastPosition.y;
                    }
                    isChanged = true;
                    continue;
                }
                if (newPosition.y > boundingBoxSize.y) {
                    if (u==0) {
                        newPosition.y = boundingBoxSize.y;
                        lastPosition.y = boundingBoxSize.y;
                    } else {
                        newPosition.y = (boundingBoxSize.y-newPosition.y)/u+boundingBoxSize.y;
                        lastPosition.y = (boundingBoxSize.y-lastPosition.y)/u+boundingBoxSize.y;
                        //newPosition.y = 2*boundingBoxSize.y-newPosition.y;
                        //lastPosition.y = 2*boundingBoxSize.y-lastPosition.y;
                    }
                    isChanged = true;
                    continue;
                }
                if (newPosition.z < -boundingBoxSize.z) {
                    if (u==0) {
                        newPosition.z = -boundingBoxSize.z;
                        lastPosition.z = -boundingBoxSize.z;
                    } else {
                        newPosition.z = (-boundingBoxSize.z-newPosition.z)/u-boundingBoxSize.z;
                        lastPosition.z = (-boundingBoxSize.z-lastPosition.z)/u-boundingBoxSize.z;
                        //newPosition.z = -2*boundingBoxSize.z-newPosition.z;
                        //lastPosition.z = -2*boundingBoxSize.z-lastPosition.z;
                    }
                    isChanged = true;
                    continue;
                }
                if (newPosition.z > boundingBoxSize.z) {
                    if (u==0) {
                        newPosition.z = boundingBoxSize.z;
                        lastPosition.z = boundingBoxSize.z;
                    } else {
                        newPosition.z = (boundingBoxSize.z-newPosition.z)/u+boundingBoxSize.z;
                        lastPosition.z = (boundingBoxSize.z-lastPosition.z)/u+boundingBoxSize.z;
                        //newPosition.z = 2*boundingBoxSize.z-newPosition.z;
                        //lastPosition.z = 2*boundingBoxSize.z-lastPosition.z;
                    }
                    isChanged = true;
                    continue;
                }
                
            } while (isChanged && count<=3);
            
            controlPoints[i].position = newPosition;
            controlPointsLastPositions[i] = lastPosition;
        }
        
        
        for (int i=0; i<8; i++) {
            //printf("point %d force: (%f, %f, %f)\n", i, pointsForces[64+i].x, pointsForces[i].y, pointsForces[i].z);
            controlPointsLastPositions[64+i] = controlPoints[64+i].position;
            controlPoints[64+i].position = framePoints[i].position;
        }
        //printf("--------------------------------------------------------\n");
        
        s += M_PI_4*delta;
    }
    controlPointsMesh->setVertexes(controlPoints);
    springLinessMesh->setVertexes(controlPoints);
    updateBezierCubeModel();
    updateBezierSphereModel();
    updateGummyModel();
}



void Controller::GummyController::updateBezierCubeModel() {
    if (!bezierCubeModel->isEnabled()) {
        return;
    }
    int counter = 0;
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints[counter].position = getBezierVertexPosition(0, (float)i/(float)bezierMeshDensity, (float)j/(float)bezierMeshDensity);
            bezierCubePoints[counter].normal = glm::vec3(0);
            counter++;
        }
    }
    
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints[counter].position = getBezierVertexPosition(1, (float)i/(float)bezierMeshDensity, (float)j/(float)bezierMeshDensity);
            bezierCubePoints[counter].normal = glm::vec3(0);
            counter++;
        }
    }
    
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints[counter].position = getBezierVertexPosition((float)i/(float)bezierMeshDensity, 0, (float)j/(float)bezierMeshDensity);
            bezierCubePoints[counter].normal = glm::vec3(0);
            counter++;
        }
    }
    
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints[counter].position = getBezierVertexPosition((float)i/(float)bezierMeshDensity, 1, (float)j/(float)bezierMeshDensity);
            bezierCubePoints[counter].normal = glm::vec3(0);
            counter++;
        }
    }
    
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints[counter].position = getBezierVertexPosition((float)i/(float)bezierMeshDensity, (float)j/(float)bezierMeshDensity, 1);
            bezierCubePoints[counter].normal = glm::vec3(0);
            counter++;
        }
    }
    
    for (int i=0; i<=bezierMeshDensity; i++) {
        for (int j=0; j<=bezierMeshDensity; j++) {
            bezierCubePoints[counter].position = getBezierVertexPosition((float)i/(float)bezierMeshDensity, (float)j/(float)bezierMeshDensity, 0);
            bezierCubePoints[counter].normal = glm::vec3(0);
            counter++;
        }
    }
    
    for (auto &triangle : bezierCubeTriangles) {
        glm::vec3 vectorU = bezierCubePoints[triangle.indices.v[1]].position - bezierCubePoints[triangle.indices.v[0]].position;
        glm::vec3 vectorV = bezierCubePoints[triangle.indices.v[2]].position - bezierCubePoints[triangle.indices.v[0]].position;
        
        glm::vec3 triangleNormal = glm::cross(vectorU, vectorV);
        
        bezierCubePoints[triangle.indices.v[0]].normal += triangleNormal;
        bezierCubePoints[triangle.indices.v[1]].normal += triangleNormal;
        bezierCubePoints[triangle.indices.v[2]].normal += triangleNormal;
    }
    
    for (auto &vertex:bezierCubePoints) {
        if (glm::length(vertex.normal)>0.01) {
            vertex.normal = glm::normalize(vertex.normal);
        }
    }
    
    bezierCubeMesh->setVertexes(bezierCubePoints);
}

void Controller::GummyController::updateBezierSphereModel() {
    if (!bezierSphereModel->isEnabled()) {
        return;
    }
    
    //for (int i=0; i<bezierSphereParametrizationPoints.size(); i++) {
    //    bezierSpherePoints[i].position = getBezierVertexPosition(bezierSphereParametrizationPoints[i]);
    //}
    
    updateModelsWithThreads(&bezierSpherePoints, &bezierSphereParametrizationPoints);
    
    
    for (auto &triangle : bezierSphereTriangles) {
        glm::vec3 vectorU = bezierSpherePoints[triangle.indices.v[1]].position - bezierSpherePoints[triangle.indices.v[0]].position;
        glm::vec3 vectorV = bezierSpherePoints[triangle.indices.v[2]].position - bezierSpherePoints[triangle.indices.v[0]].position;
        
        glm::vec3 triangleNormal = glm::cross(vectorU, vectorV);
        
        bezierSpherePoints[triangle.indices.v[0]].normal += triangleNormal;
        bezierSpherePoints[triangle.indices.v[1]].normal += triangleNormal;
        bezierSpherePoints[triangle.indices.v[2]].normal += triangleNormal;
    }
    
    for (auto &vertex:bezierSpherePoints) {
        if (glm::length(vertex.normal)>0.0001) {
            vertex.normal = glm::normalize(vertex.normal);
        }
    }
    
    glm::vec3 norm = glm::vec3(0);
    for(int i=0; i<=bezierMeshDensity; i++) {
        norm += bezierSpherePoints[i].normal;
    }
    norm = glm::normalize(norm);
    for(int i=0; i<=bezierMeshDensity; i++) {
        bezierSpherePoints[i].normal = norm;
    }
    
    norm = glm::vec3(0);
    for(int i=0; i<=bezierMeshDensity; i++) {
        norm += bezierSpherePoints[(bezierMeshDensity+1)*(bezierMeshDensity+1)-i-1].normal;
    }
    norm = glm::normalize(norm);
    for(int i=0; i<=bezierMeshDensity; i++) {
        bezierSpherePoints[(bezierMeshDensity+1)*(bezierMeshDensity+1)-i-1].normal = norm;
    }
    
    bezierSphereMesh->setVertexes(bezierSpherePoints);
}

void Controller::GummyController::updateGummyModel() {
    if (!gummyModel->isEnabled()) {
        return;
    }
    
    //for (int i=0; i<gummyParametrizationPoints.size(); i++) {
    //    gummyPoints[i].position = getBezierVertexPosition(gummyParametrizationPoints[i]);
    //}
    
    updateModelsWithThreads(&gummyPoints, &gummyParametrizationPoints);
    
    for (auto &triangle : gummyTriangles) {
        glm::vec3 vectorU = gummyPoints[triangle.indices.v[1]].position - gummyPoints[triangle.indices.v[0]].position;
        glm::vec3 vectorV = gummyPoints[triangle.indices.v[2]].position - gummyPoints[triangle.indices.v[0]].position;
        
        glm::vec3 triangleNormal = glm::cross(vectorU, vectorV);
        triangleNormal = glm::normalize(triangleNormal);
        gummyPoints[triangle.indices.v[0]].normal += triangleNormal;
        gummyPoints[triangle.indices.v[1]].normal += triangleNormal;
        gummyPoints[triangle.indices.v[2]].normal += triangleNormal;
    }
    
    for (auto &vertex:gummyPoints) {
        if (glm::length(vertex.normal)>0.0001) {
            vertex.normal = glm::normalize(vertex.normal);
        }
    }
    
    gummyMesh->setVertexes(gummyPoints);
}

void Controller::GummyController::updateModelsWithThreads(std::vector<Mesh::Vertex>* positionsVector, std::vector<glm::vec3>* parametrization) {
    tempThread0Positions.clear();
    tempThread1Positions.clear();
    tempThread2Positions.clear();
    tempThread3Positions.clear();
    tempThread4Positions.clear();
    tempThread5Positions.clear();
    tempThread6Positions.clear();
    tempThread7Positions.clear();
    
    std::vector<std::thread> threads = std::vector<std::thread>();
    
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread0Positions, parametrization, 0));
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread1Positions, parametrization, 1));
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread2Positions, parametrization, 2));
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread3Positions, parametrization, 3));
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread4Positions, parametrization, 4));
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread5Positions, parametrization, 5));
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread6Positions, parametrization, 6));
    threads.push_back(std::thread (&GummyController::updateModelFromThread, this, &tempThread7Positions, parametrization, 7));
    
    
    for (auto &thread : threads) {
        thread.join();
    }
    
    
    int i=0;
    for (auto &pos : tempThread0Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    for (auto &pos : tempThread1Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    for (auto &pos : tempThread2Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    for (auto &pos : tempThread3Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    for (auto &pos : tempThread4Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    for (auto &pos : tempThread5Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    for (auto &pos : tempThread6Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    for (auto &pos : tempThread7Positions) {
        (*positionsVector)[i].position = pos;
        i++;
    }
    
}

void Controller::GummyController::updateModelFromThread(std::vector<glm::vec3>* tempPositionsVector, std::vector<glm::vec3>* parametrization, int thread) {
    
    int s = (int)(*parametrization).size()/8;
    
    for (int i=0; i<s; i++) {
        (*tempPositionsVector).push_back(getBezierVertexPosition((*parametrization)[i+s*thread]));
    }
    if (thread == 7) {
        for (int i=s*thread+s; i<(*parametrization).size(); i++) {
            (*tempPositionsVector).push_back(getBezierVertexPosition((*parametrization)[i]));
        }
    }
    
}


void Controller::GummyController::setFramePointsPositions(glm::vec3 center, glm::vec3 rotation) {
    boxCenter = center;
    boxRotation = rotation;
    
    
    glm::mat4 rotationMatrix = glm::mat4();
    rotationMatrix = glm::rotate(rotationMatrix, rotation.x, glm::vec3(1,0,0));
    rotationMatrix = glm::rotate(rotationMatrix, rotation.y, glm::vec3(0,1,0));
    rotationMatrix = glm::rotate(rotationMatrix, rotation.z, glm::vec3(0,0,1));
    
    glm::vec4 point;
    
    point = glm::vec4(-frameSize/2, -frameSize/2, -frameSize/2, 1) * rotationMatrix;
    framePoints[0].position = center + glm::vec3(point);
    point = glm::vec4(-frameSize/2, -frameSize/2, frameSize/2, 1) * rotationMatrix;
    framePoints[1].position = center + glm::vec3(point);
    point = glm::vec4(-frameSize/2, frameSize/2, frameSize/2, 1) * rotationMatrix;
    framePoints[2].position = center + glm::vec3(point);
    point = glm::vec4(-frameSize/2, frameSize/2, -frameSize/2, 1) * rotationMatrix;
    framePoints[3].position = center + glm::vec3(point);
    
    point = glm::vec4(frameSize/2, -frameSize/2, -frameSize/2, 1) * rotationMatrix;
    framePoints[4].position = center + glm::vec3(point);
    point = glm::vec4(frameSize/2, -frameSize/2, frameSize/2, 1) * rotationMatrix;
    framePoints[5].position = center + glm::vec3(point);
    point = glm::vec4(frameSize/2, frameSize/2, frameSize/2, 1) * rotationMatrix;
    framePoints[6].position = center + glm::vec3(point);
    point = glm::vec4(frameSize/2, frameSize/2, -frameSize/2, 1) * rotationMatrix;
    framePoints[7].position = center + glm::vec3(point);
    
    
    
    //framePoints[1].position = center + glm::vec3(-frameSize/2, -frameSize/2, frameSize/2);
    //framePoints[2].position = center + glm::vec3(-frameSize/2, frameSize/2, frameSize/2);
    //framePoints[3].position = center + glm::vec3(-frameSize/2, frameSize/2, -frameSize/2);
    
    //framePoints[4].position = center + glm::vec3(frameSize/2, -frameSize/2, -frameSize/2);
    //framePoints[5].position = center + glm::vec3(frameSize/2, -frameSize/2, frameSize/2);
    //framePoints[6].position = center + glm::vec3(frameSize/2, frameSize/2, frameSize/2);
    //framePoints[7].position = center + glm::vec3(frameSize/2, frameSize/2, -frameSize/2);
    
    frameLinessMesh->setVertexes(framePoints);
}


Model::Model* Controller::GummyController::getControlPointsModel() {
    return controlPointsModel;
}

Model::Model* Controller::GummyController::getSpringLinesModel() {
    return springLinesModel;
}

Model::Model* Controller::GummyController::getFrameModel() {
    return frameLineModel;
}

Model::Model* Controller::GummyController::getBoundingBoxModel() {
    return boundingBoxModel;
}

Model::Model* Controller::GummyController::getBezierCubeModel() {
    return bezierCubeModel;
}

Model::Model* Controller::GummyController::getBezierSphereModel() {
    return bezierSphereModel;
}

Model::Model* Controller::GummyController::getGummyModel() {
    return gummyModel;
}


void Controller::GummyController::moveBox(float deltaX, float deltaY, float deltaZ) {
    setFramePointsPositions(boxCenter + glm::vec3(deltaX, deltaY, deltaZ), boxRotation);
}

void Controller::GummyController::rotateBox(float deltaX, float deltaY, float deltaZ) {
    setFramePointsPositions(boxCenter, boxRotation + glm::vec3(deltaX, deltaY, deltaZ));
}

glm::vec3 Controller::GummyController::getBezierVertexPosition(glm::vec3 vec) {
    return getBezierVertexPosition(vec.x, vec.y, vec.z);
}

glm::vec3 Controller::GummyController::getBezierVertexPosition(float u, float v, float w) {
    
    u = clamp(u, 0.0f, 1.0f);
    v = clamp(v, 0.0f, 1.0f);
    w = clamp(w, 0.0f, 1.0f);
    
    std::vector<glm::vec3> tempControlPointsU = std::vector<glm::vec3>(4);
    std::vector<glm::vec3> tempControlPointsV = std::vector<glm::vec3>(4);
    std::vector<glm::vec3> tempControlPointsW = std::vector<glm::vec3>(4);
    
    tempControlPointsU.clear();
    for (int i=0; i<4; i++) {
        
        tempControlPointsV.clear();
        for (int j=0; j<4; j++) {
            tempControlPointsW.clear();
            for (int k=0; k<4; k++) {
                tempControlPointsW.push_back(controlPoints[i*16+j*4+k].position);
            }
            tempControlPointsV.push_back(getPointForParameterAndPoints(w, tempControlPointsW));
        }
        tempControlPointsU.push_back(getPointForParameterAndPoints(v, tempControlPointsV));
    }
    glm::vec3 retVec = getPointForParameterAndPoints(u, tempControlPointsU);
    return retVec + glm::vec3(0);
}

glm::vec3 Controller::GummyController::getPointForParameterAndPoints(float param, std::vector<glm::vec3> tempControlPoints) {
    
    glm::vec4 paramVec = glm::vec4(1, param, param*param, param*param*param);
    glm::vec4 coefficients = bernsteinMatrix*paramVec;
    glm::vec3 sum = glm::vec3(0);
    
    for (int i=0; i<4; i++) {
        sum += coefficients[i]*tempControlPoints[i];
    }
    return sum;
}

void Controller::GummyController::reset() {
    setFramePointsPositions(glm::vec3(0), glm::vec3(0));
    
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            for (int k=0; k<4; k++) {
                glm::vec3 point = vec3(-0.5f+i*1.0f/3.0f, -0.5f+j*1.0f/3.0f, -0.5f+k*1.0f/3.0f);
                controlPoints[i*16+j*4+k].position = point;
                controlPointsLastPositions[i*16+j*4+k] = point + glm::vec3(randDisplacement*(rand()%2000-1000)/1000.0f,randDisplacement*(rand()%2000-1000)/1000.0f,randDisplacement*(rand()%2000-1000)/1000.0f);
                pointsForces[i*16+j*4+k] = glm::vec3(0);
            }
        }
    }
    
    for (int i=0; i<8; i++) {
        controlPoints[64+i].position = framePoints[i].position;
        controlPointsLastPositions[64+i] = framePoints[i].position + glm::vec3(randDisplacement*(rand()%2000-1000)/1000.0f,randDisplacement*(rand()%2000-1000)/1000.0f,randDisplacement*(rand()%2000-1000)/1000.0f);
        pointsForces[64+i] = glm::vec3(0);
    }
}


#pragma mark -
#pragma mark Enabling/Disabling Models

void Controller::GummyController::enableSpringLines() {
    springLinesModel->enableModel();
}

void Controller::GummyController::disableSpringLines() {
    springLinesModel->disableModel();
}

void Controller::GummyController::enableDots() {
    controlPointsModel -> enableModel();
}

void Controller::GummyController::disableDots() {
    controlPointsModel -> disableModel();
}

void Controller::GummyController::enableBoundingBox() {
    boundingBoxModel->enableModel();
}

void Controller::GummyController::disableBoundingBox() {
    boundingBoxModel->disableModel();
}

void Controller::GummyController::enableBezierCube() {
    bezierCubeModel->enableModel();
}

void Controller::GummyController::disableBezierCube() {
    bezierCubeModel->disableModel();
}

void Controller::GummyController::enableBezierSphere() {
    bezierSphereModel->enableModel();
}

void Controller::GummyController::disableBezierSphere() {
    bezierSphereModel->disableModel();
}

void Controller::GummyController::enableGummyBear() {
    gummyModel->enableModel();
}

void Controller::GummyController::disableGummyBear() {
    gummyModel->disableModel();
}
