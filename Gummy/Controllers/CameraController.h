//
//  CameraController.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__CameraController__
#define __Gummy__CameraController__

#include <iostream>

#include "glm.hpp"

namespace Controller {
    class CameraController {
        
        glm::vec3 m_position = glm::vec3(0.0f, 0.0f, -5.0f);
        float m_longitude;
        float m_latitude;
        float m_aspect;
        
    public:
        void mouseDrag(int x, int y);
        void keyboardMove(float x, float y, float z);
        void setAspectRatio(float aspect);
        void rotateAroundPoint(glm::vec3 point, float x, float y);
        
        glm::vec3 getPosition();
        glm::mat4 getViewMatrix();
        glm::mat4 getProjectionMatrix();
        glm::vec3 getCameraDirection();
    };
}
#endif /* defined(__Gummy__CameraController__) */
