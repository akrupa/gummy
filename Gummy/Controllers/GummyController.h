//
//  GummyController.h
//  Gummy
//
//  Created by Adrian Krupa on 23.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__GummyController__
#define __Gummy__GummyController__

#include <stdio.h>
#include <vector>

#include "Vertex.h"
#include "Line.h"
#include "PointsMesh.h"
#include "LineMesh.h"
#include "TriangleMesh.h"
#include "Model.h"

namespace Controller {
    class GummyController {
        float s;
        
        bool isInited;
        //float l;
        float l0;
        float lFrame0;
        float bezierMeshDensity;
        
        
        
        
        float frameSize = 1.0f;
        
        glm::mat4 bernsteinMatrix;
        
        glm::vec3 boxCenter;
        glm::vec3 boxRotation;
        glm::vec3 boundingBoxSize;
        
        std::vector<glm::vec3> controlPointsLastPositions;
        std::vector<glm::vec3> springLengthLastPosition;
        std::vector<float> springsBaseLength;
        std::vector<Mesh::Vertex> controlPoints;
        Mesh::PointsMesh *controlPointsMesh;
        Model::Model *controlPointsModel;
        
        std::vector<Mesh::Line> springLines;
        Mesh::LineMesh *springLinessMesh;
        Model::Model *springLinesModel;
        
        std::vector<glm::vec3> pointsForces;
        
        std::vector<Mesh::Vertex> framePoints;
        std::vector<Mesh::Line> frameLines;
        Mesh::LineMesh *frameLinessMesh;
        Model::Model *frameLineModel;
        
        std::vector<Mesh::Vertex>boundingBoxPoints;
        std::vector<Mesh::Triangle> boundingBoxTriangles;
        Mesh::TriangleMesh *boundingBoxMesh;
        Model::Model *boundingBoxModel;
        
        std::vector<Mesh::Vertex>bezierCubePoints;
        std::vector<Mesh::Triangle>bezierCubeTriangles;
        Mesh::Mesh *bezierCubeMesh;
        Model::Model *bezierCubeModel;
        
        std::vector<Mesh::Vertex>bezierSpherePoints;
        std::vector<Mesh::Triangle>bezierSphereTriangles;
        std::vector<glm::vec3>bezierSphereParametrizationPoints;
        Mesh::Mesh *bezierSphereMesh;
        Model::Model *bezierSphereModel;
        
        std::vector<Mesh::Vertex>gummyPoints;
        std::vector<Mesh::Triangle>gummyTriangles;
        std::vector<glm::vec3>gummyParametrizationPoints;
        Mesh::Mesh *gummyMesh;
        Model::Model *gummyModel;
        
        std::vector<glm::vec3>tempThread0Positions = std::vector<glm::vec3>();
        std::vector<glm::vec3>tempThread1Positions = std::vector<glm::vec3>();
        std::vector<glm::vec3>tempThread2Positions = std::vector<glm::vec3>();
        std::vector<glm::vec3>tempThread3Positions = std::vector<glm::vec3>();
        std::vector<glm::vec3>tempThread4Positions = std::vector<glm::vec3>();
        std::vector<glm::vec3>tempThread5Positions = std::vector<glm::vec3>();
        std::vector<glm::vec3>tempThread6Positions = std::vector<glm::vec3>();
        std::vector<glm::vec3>tempThread7Positions = std::vector<glm::vec3>();

        
        void setFramePointsPositions(glm::vec3 center, glm::vec3 rotation);
        glm::vec3 getBezierVertexPosition(glm::vec3 vec);
        glm::vec3 getBezierVertexPosition(float u, float v, float w);
        glm::vec3 getPointForParameterAndPoints(float param, std::vector<glm::vec3> tempControlPoints);
        
        void initVectors();
        void initSimulationParameters();
        void initFrameLinesModel();
        void initControlPointsModel();
        void initSpringsLinesModel();
        void initBoundingBoxModel();
        void initBezierCubeModel();
        void initBezierSphereModel();
        void initGummyModel();
        
        void updateBezierCubeModel();
        void updateBezierSphereModel();
        void updateGummyModel();
        void updateModelsWithThreads(std::vector<Mesh::Vertex>* positionsVector, std::vector<glm::vec3>* parametrization);
        void updateModelFromThread(std::vector<glm::vec3>* tempPositionsVector, std::vector<glm::vec3>* parametrization, int thread);
        
    public:
        void init();
        void update();
        void reset();
        
        float c1;
        float c2;
        float k;
        float m;
        float u;
        float delta;
        float randDisplacement;

        Model::Model* getControlPointsModel();
        Model::Model* getSpringLinesModel();
        Model::Model* getFrameModel();
        Model::Model* getBoundingBoxModel();
        Model::Model* getBezierCubeModel();
        Model::Model* getBezierSphereModel();
        Model::Model* getGummyModel();
        
        void moveBox(float deltaX, float deltaY, float deltaZ);
        void rotateBox(float deltaX, float deltaY, float deltaZ);
        
        void enableSpringLines();
        void disableSpringLines();
        
        void enableDots();
        void disableDots();
        
        void enableBoundingBox();
        void disableBoundingBox();
        
        void enableBezierCube();
        void disableBezierCube();
        
        void enableBezierSphere();
        void disableBezierSphere();
        
        void enableGummyBear();
        void disableGummyBear();
    };
}

#endif /* defined(__Gummy__GummyController__) */
