//
//  AKController.h
//  Gummy
//
//  Created by Adrian Krupa on 24.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AKOpenGLView.h"
#import "Renderer.h"
#import "CameraController.h"

@class AKOpenGLView;

@interface AKController : NSObject {
    Renderer::Renderer *renderer;
    Controller::CameraController *cameraController;
}

@property (nonatomic, strong) IBOutlet AKOpenGLView *openGLView;


@property (nonatomic, strong) IBOutlet NSTextField *c1TextField;
@property (nonatomic, strong) IBOutlet NSTextField *c2TextField;
@property (nonatomic, strong) IBOutlet NSTextField *kTextField;
@property (nonatomic, strong) IBOutlet NSTextField *mTextField;
@property (nonatomic, strong) IBOutlet NSTextField *uTextField;
@property (nonatomic, strong) IBOutlet NSTextField *simulationQualityTextField;
@property (nonatomic, strong) IBOutlet NSTextField *randTextField;

@property (nonatomic, assign) BOOL isInited;

@end
