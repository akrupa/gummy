//
//  CameraController.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "CameraController.h"

#include "matrix_transform.hpp"

using namespace glm;

namespace Controller {
    
    void CameraController::mouseDrag(int x, int y)
    {
        const float Scale = 0.005;
        if (glm::abs(m_longitude) != (float)M_PI_2) {
            m_latitude += x * Scale;
        }
        m_longitude += y * Scale;
        
        m_longitude = clamp<float>(m_longitude, -M_PI_2, M_PI_2);
    }
    
    void CameraController::keyboardMove(float x, float y, float z)
    {
        const float Scale = 0.04f;
        
        vec3 diff = Scale * vec3(x, y, z);
        
        mat4 latRotation = glm::rotate(mat4(), -m_latitude, vec3(0.0f, 1.0f, 0.0f));
        mat4 longRotation = glm::rotate(mat4(), -m_longitude, vec3(1.0f, 0.0f, 0.0f));
        
        m_position += vec3(latRotation * longRotation * vec4(diff, 0.0f));
    }
    
    void CameraController::rotateAroundPoint(glm::vec3 point, float x, float y) {
        m_position = vec3((point.x + (m_position.x-point.x)*cos(x) - (m_position.z - point.z)*sin(x)), m_position.y, (point.z + (m_position.x-point.x)*sin(x) + (m_position.z - point.z)*cos(x)));
        
        mat4 m = glm::lookAt(m_position, point, vec3(0,1,0));
        m_latitude = M_PI_2 + glm::atan(m[0].x, m[0].z);
        m_longitude = glm::acos(m[1].y);
    }
    
    
    vec3 CameraController::getPosition()
    {
        return m_position;
    }
    
    mat4 CameraController::getViewMatrix()
    {
        
        mat4 translation = translate(mat4(), m_position);
        mat4 latRotation = glm::rotate(mat4(), m_latitude, vec3(0.0f, 1.0f, 0.0f));
        mat4 longRotation = glm::rotate(mat4(), m_longitude, vec3(1.0f, 0.0f, 0.0f));
        
        return longRotation * latRotation * translation;
        //return glm::lookAt(m_position, vec3(0,0,0), vec3(0,1,0));
    }
    
    mat4 CameraController::getProjectionMatrix()
    {
        mat4 perspective = glm::perspective((float)M_PI_4, m_aspect, 0.1f, 1000.0f);
        
        return perspective;
    }
    
    void CameraController::setAspectRatio(float aspect) {
        m_aspect = aspect;
    }
    
    glm::vec3 CameraController::getCameraDirection() {
        glm::vec3 v = glm::vec3(glm::cos(m_longitude) * glm::sin(m_latitude),
                                -glm::sin(m_longitude),
                                glm::cos(m_longitude) * glm::cos(m_latitude));
        //printf("%f - %f\n", m_latitude, m_longitude);
        
        
        return glm::normalize(v);
    }

}
