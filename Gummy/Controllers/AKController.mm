//
//  AKController.m
//  Gummy
//
//  Created by Adrian Krupa on 24.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import "AKController.h"
#import "GummyController.h"
#import "PointsMesh.h"
#import "Mesh.h"
#import "DiffuseShader.h"

@interface AKController () {
    Controller::GummyController *_gummyController;
}

@end

@implementation AKController

- (id)init
{
    self = [super init];
    if (self) {
        _gummyController = new Controller::GummyController();
        [self setIsInited:NO];
    }

    
    return self;
}

- (void) awakeFromNib {

    __typeof(self) __weak weakSelf = self;
    [_openGLView setHandlerForUpdate:^{
        if (![weakSelf isInited]) {
            [weakSelf initialize];
        }
        [weakSelf update];
    }];
    
    [_openGLView setHandlerForBoxMove:^(float deltaX, float deltaY) {
        if (![weakSelf isInited]) {
            [weakSelf initialize];
        }
        [weakSelf moveBoxWithX:deltaX andWithY:deltaY];
    }];
    
    [_openGLView setHandlerForBoxRotate:^(float deltaX, float deltaY) {
        if (![weakSelf isInited]) {
            [weakSelf initialize];
        }
        [weakSelf rotateBoxWithX:deltaX andWithY:deltaY];
    }];
}

- (void)update {
    _gummyController->update();
}

- (void)moveBoxWithX:(float)deltaX andWithY:(float)deltaY {
    glm::vec3 cameraDirection = cameraController->getCameraDirection();
    //NSLog(@"(%f, %f, %f)", cameraDirection.x, cameraDirection.y, cameraDirection.z);
    _gummyController -> moveBox(deltaX*cameraDirection.z, deltaY, deltaX*cameraDirection.x);
}

- (void)rotateBoxWithX:(float)deltaX andWithY:(float)deltaY {
    glm::vec3 cameraDirection = cameraController->getCameraDirection();
    _gummyController->rotateBox(deltaX*cameraDirection.x, deltaY, deltaX*cameraDirection.z);
}


- (void)initialize {
    [self setIsInited:YES];
    cameraController = new Controller::CameraController();

    renderer = new Renderer::Renderer();
    renderer->setup();
    renderer->initScene();
    [_openGLView setCameraController:cameraController];
    [_openGLView setRenderer:renderer];
    _gummyController->init();
    
    renderer->addModel(_gummyController->getControlPointsModel());
    renderer->addModel(_gummyController->getSpringLinesModel());
    renderer->addModel(_gummyController->getFrameModel());
    renderer->addModel(_gummyController->getBoundingBoxModel());
    renderer->addModel(_gummyController->getBezierCubeModel());
    renderer->addModel(_gummyController->getBezierSphereModel());
    renderer->addModel(_gummyController->getGummyModel());
}

- (IBAction)displaySpringLinesCheckboxChanged:(id)sender {
    if ([sender state] == NSOnState) {
        _gummyController->enableSpringLines();
    } else {
        _gummyController->disableSpringLines();
    }
}

- (IBAction)displayDotsCheckboxChanged:(id)sender {
    if ([sender state] == NSOnState) {
        _gummyController->enableDots();
    } else {
        _gummyController->disableDots();
    }
}

- (IBAction)displayBezierCubeCheckboxChanged:(id)sender {
    if ([sender state] == NSOnState) {
        _gummyController->enableBezierCube();
    } else {
        _gummyController->disableBezierCube();
    }
}

- (IBAction)displayBezierSphereCheckboxChanged:(id)sender {
    if ([sender state] == NSOnState) {
        _gummyController->enableBezierSphere();
    } else {
        _gummyController->disableBezierSphere();
    }
}

- (IBAction)displayGummyBearCheckboxChanged:(id)sender {
    if ([sender state] == NSOnState) {
        _gummyController->enableGummyBear();
    } else {
        _gummyController->disableGummyBear();
    }
}

- (IBAction)lostFocus:(id)sender {
    [_openGLView setFirstResponder];
}

- (IBAction)reset:(id)sender {
    _gummyController->randDisplacement = [_randTextField floatValue];
    _gummyController->reset();
}

- (IBAction)applyClicked:(id)sender {
    _gummyController->m = [_mTextField floatValue];
    _gummyController->k = [_kTextField floatValue];
    _gummyController->c1 = [_c1TextField floatValue];
    _gummyController->c2 = [_c2TextField floatValue];
    _gummyController->delta = 1.0f/([_simulationQualityTextField integerValue]*60.0f);
    _gummyController->u = [_uTextField floatValue];
}

@end
