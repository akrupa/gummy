//
//  Renderer.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__Renderer__
#define __Gummy__Renderer__

#import <OpenGL/gl3.h>
#import "glm.hpp"
#import <vector>
#import "Model.h"
#import "CameraController.h"

namespace Renderer {
    class Renderer {
        
        std::vector<Model::Model *> models;

    public:
        void setup();
        void initScene();
        void renderScene();
        void renderWithCameraController(Controller::CameraController *camera);
        void addModel(Model::Model* model);
    };
}
#endif /* defined(__Gummy__Renderer__) */
