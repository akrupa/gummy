//
//  main.m
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
