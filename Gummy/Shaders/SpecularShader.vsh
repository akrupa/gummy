#version 410 core

uniform mat4 viewProjectionMatrix;
uniform mat4 modelMatrix;

in vec3 in_Position;
in vec3 in_Normal;
in vec4 in_Color;

out vec4 pass_Normal;
out vec4 pass_Color;
out vec4 pass_Position;

void main(void)
{
    vec4 normal = modelMatrix * vec4(in_Normal, 1.0);
    vec4 worldPosition = modelMatrix * vec4(in_Position, 1.0);
    
    pass_Position = worldPosition;
    pass_Normal = normal;
    pass_Color = in_Color;
    
    gl_Position = viewProjectionMatrix * worldPosition;
}