//
//  SpecularShader.cpp
//  Gummy
//
//  Created by Adrian Krupa on 08.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "SpecularShader.h"

std::string Shader::SpecularShader::getShaderName() {
    return "SpecularShader";
}

void Shader::SpecularShader::bindAttributeLocations() {
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    glBindAttribLocation(m_program, VertexAttribColor, "in_Color");
}

void Shader::SpecularShader::fetchUniformLocations() {
    _viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    _modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    _ambientColorUniform = glGetUniformLocation(m_program, "ambientColor");
    _lightPositionUniform = glGetUniformLocation(m_program, "lightPosition");
    _lightPositionUniform = glGetUniformLocation(m_program, "lightColor");
}

#pragma mark IPositionShader

GLuint Shader::SpecularShader::getViewProjectionMatrixUniform() {
    return _viewProjectionMatrixUniform;
}

GLuint Shader::SpecularShader::getModelMatrixUniform() {
    return _modelMatrixUniform;
}

#pragma mark IAmbientShader

GLuint Shader::SpecularShader::getAmbientColorUniform() {
    return _ambientColorUniform;
}

#pragma mark ILightShader
GLuint Shader::SpecularShader::getLightPositionUniform() {
    return _lightPositionUniform;
}
GLuint Shader::SpecularShader::getLightColorUniform() {
    return _lightColorUniform;
}