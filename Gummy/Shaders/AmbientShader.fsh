#version 410 core

uniform vec3 ambientColor;

in vec3 pass_Color;

out vec4 out_Color;

void main(void) {
    vec3 scatteredLight = ambientColor;
	out_Color = min(vec4(pass_Color*scatteredLight, 1.0), vec4(1.0));
}