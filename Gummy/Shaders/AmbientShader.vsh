#version 410 core

uniform mat4 viewProjectionMatrix;
uniform mat4 modelMatrix;

in vec3 in_Position;
in vec3 in_Color;

out vec3 pass_Color;


void main(void)
{
    vec4 worldPosition = modelMatrix * vec4(in_Position, 1.0);
    
    pass_Color = in_Color;
    
	gl_Position = viewProjectionMatrix * worldPosition;
}