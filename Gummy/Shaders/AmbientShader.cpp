//
//  AmbientShader.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "AmbientShader.h"

std::string Shader::AmbientShader::getShaderName() {
    return "AmbientShader";
}

void Shader::AmbientShader::bindAttributeLocations() {
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribColor, "in_Color");
    
}

void Shader::AmbientShader::fetchUniformLocations() {
    _viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    _modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    _ambientColorUniform = glGetUniformLocation(m_program, "ambientColor");
}

#pragma mark IPositionShader

GLuint Shader::AmbientShader::getViewProjectionMatrixUniform() {
    return _viewProjectionMatrixUniform;
}

GLuint Shader::AmbientShader::getModelMatrixUniform() {
    return _modelMatrixUniform;
}

#pragma mark IColorableShader

GLuint Shader::AmbientShader::getAmbientColorUniform() {
    return _ambientColorUniform;
}



