#version 410 core

uniform vec3 ambientColor;
uniform vec3 lightColor;

in vec4 pass_Normal;
in vec4 pass_Color;
in vec4 pass_Position;

out vec4 out_Color;

uniform vec3 lightPosition;


void main(void) {
    
    vec3 lightDirection = normalize(lightPosition - vec3(pass_Position));
    
    float diffuse = max(0.0, dot(normalize(pass_Normal.rgb), lightDirection));
    
    vec3 scatteredLight = ambientColor*vec3(0.6,0.3,0) + lightColor * diffuse;
    //vec3 scatteredLight = lightColor * diffuse;
    vec3 rgb = min(pass_Color.rgb * scatteredLight, vec3(1.0));
    //out_Color =  vec4(rgb, pass_Color.a);
    out_Color =  vec4(rgb, pass_Color.a);
    //vec4 scatteredLight = vec4(ambientColor, 1.0);
    //out_Color = min(pass_Color*scatteredLight, vec4(1.0));
}