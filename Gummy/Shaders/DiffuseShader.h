//
//  DiffuseShader.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__DiffuseShader__
#define __Gummy__DiffuseShader__

#include <stdio.h>
#include "ShadersInterfaces.h"
#include "Shader.h"

namespace Shader {
    class DiffuseShader : public Shader, public IPositionShader, public IAmbientShader, public ILightShader {
        
    private:
        GLuint _viewProjectionMatrixUniform;
        GLuint _modelMatrixUniform;
        GLuint _ambientColorUniform;
        GLuint _lightPositionUniform;
        GLuint _lightColorUniform;
    
    protected:
        virtual std::string getShaderName();
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
        
    public:
        
        // IPositionShader
        virtual GLuint getModelMatrixUniform();
        virtual GLuint getViewProjectionMatrixUniform();

        // IAmbientShader
        virtual GLuint getAmbientColorUniform();
        
        // ILightShader
        virtual GLuint getLightPositionUniform();
        virtual GLuint getLightColorUniform();
    };
}
#endif /* defined(__Gummy__DiffuseShader__) */
