//
//  Shader.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__Shader__
#define __Gummy__Shader__

#include <iostream>
#include "ShadersInterfaces.h"
#include <OpenGL/gl3.h>

typedef enum : GLuint {
    VertexAttribPosition,
    VertexAttribNormal,
    VertexAttribColor
} VertexAttrib;

namespace Shader {
    class Shader {
        
    protected:
        GLuint m_program;
        
        bool compileShader(GLuint *shader, GLenum type, std::string filePath);
        bool linkProgram(GLuint prog);
        
        virtual std::string getShaderName();
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
        

    public:
        Shader();
        ~Shader();
        
        
        virtual void setup();
        GLuint getProgram();
    };
}

#endif /* defined(__Gummy__Shader__) */
