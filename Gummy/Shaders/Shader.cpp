//
//  Shader.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "Shader.h"

#include <string>
#include <fstream>
#include <streambuf>

Shader::Shader::Shader()
{
    
}



Shader::Shader::~Shader()
{
    if (m_program) {
        glDeleteProgram(m_program);
        m_program = 0;
    }
}

void Shader::Shader::setup()
{
    GLuint vertShader, fragShader;
    std::string vertShaderPathname, fragShaderPathname;
    
    
    m_program = glCreateProgram();
    
    vertShaderPathname = getShaderName() + ".vsh";
#if defined(__APPLE__) && !defined(DEBUG)
    vertShaderPathname = "../../../" + vertShaderPathname;
#endif
    if (!compileShader(&vertShader, GL_VERTEX_SHADER, vertShaderPathname)) {
        std::cout << "Failed to compile vertex shader: " + getShaderName();
        return;
    }
    
    fragShaderPathname = getShaderName() + ".fsh";
#if defined(__APPLE__) && !defined(DEBUG)
    fragShaderPathname = "../../../" + fragShaderPathname;
#endif
    if (!compileShader(&fragShader, GL_FRAGMENT_SHADER, fragShaderPathname)) {
        std::cout << "Failed to compile fragment shader: " + getShaderName();
        return;
    }
    
    
    glAttachShader(m_program, vertShader);
    glAttachShader(m_program, fragShader);
    
    
    bindAttributeLocations();
    
    
    if (!linkProgram(m_program)) {
        std::cout << "Failed to link program: " + getShaderName();
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (m_program) {
            glDeleteProgram(m_program);
            m_program = 0;
        }
    }
    
    fetchUniformLocations();
    
    
    if (vertShader) {
        glDetachShader(m_program, vertShader);
        glDeleteShader(vertShader);
    }
    
    if (fragShader) {
        glDetachShader(m_program, fragShader);
        glDeleteShader(fragShader);
    }
}

GLuint Shader::Shader::getProgram()
{
    return m_program;
}


std::string Shader::Shader::getShaderName()
{
    return "DONT_CALL_ME!";
}

void Shader::Shader::bindAttributeLocations()
{
    
}

void Shader::Shader::fetchUniformLocations()
{
    
}


bool Shader::Shader::compileShader(GLuint *shader, GLenum type, std::string filePath)
{
    GLint status;
    
    std::ifstream fileStream;
    fileStream.open(filePath, std::ios_base::in);

    std::string contents((std::istreambuf_iterator<char>(fileStream)),
                         std::istreambuf_iterator<char>());
    
    fileStream.close();
    
    const GLchar *source = contents.c_str();
        
    if (!source) {
        std::cout << "Failed to load shader: " + filePath;
        return false;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        std::cout << "Shader compile log: " << std::endl << log;
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return false;
    }
    
    return true;
}

bool Shader::Shader::linkProgram(GLuint prog)
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        std::cout << "Program link log: " << std::endl << log;
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return false;
    }
    
    return true;
}


