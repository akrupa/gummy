//
//  DiffuseShader.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "DiffuseShader.h"

std::string Shader::DiffuseShader::getShaderName() {
    return "DiffuseShader";
}

void Shader::DiffuseShader::bindAttributeLocations() {
    glBindAttribLocation(m_program, VertexAttribPosition, "in_Position");
    glBindAttribLocation(m_program, VertexAttribNormal, "in_Normal");
    glBindAttribLocation(m_program, VertexAttribColor, "in_Color");
}

void Shader::DiffuseShader::fetchUniformLocations() {
    _viewProjectionMatrixUniform = glGetUniformLocation(m_program, "viewProjectionMatrix");
    _modelMatrixUniform = glGetUniformLocation(m_program, "modelMatrix");
    _ambientColorUniform = glGetUniformLocation(m_program, "ambientColor");
    _lightPositionUniform = glGetUniformLocation(m_program, "lightPosition");
    _lightColorUniform= glGetUniformLocation(m_program, "lightColor");
}

#pragma mark IPositionShader

GLuint Shader::DiffuseShader::getViewProjectionMatrixUniform() {
    return _viewProjectionMatrixUniform;
}

GLuint Shader::DiffuseShader::getModelMatrixUniform() {
    return _modelMatrixUniform;
}

#pragma mark IAmbientShader

GLuint Shader::DiffuseShader::getAmbientColorUniform() {
    return _ambientColorUniform;
}

#pragma mark ILightShader
GLuint Shader::DiffuseShader::getLightPositionUniform() {
    return _lightPositionUniform;
}
GLuint Shader::DiffuseShader::getLightColorUniform() {
    return _lightColorUniform;
}