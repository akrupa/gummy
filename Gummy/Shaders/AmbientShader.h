//
//  AmbientShader.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__AmbientShader__
#define __Gummy__AmbientShader__

#include <stdio.h>
#include "ShadersInterfaces.h"
#include "Shader.h"

namespace Shader {
    class AmbientShader : public Shader, public IPositionShader, public IAmbientShader {
        
    private:
        GLuint _viewProjectionMatrixUniform;
        GLuint _modelMatrixUniform;
        GLuint _ambientColorUniform;
        
    protected:
        virtual std::string getShaderName();
        virtual void bindAttributeLocations();
        virtual void fetchUniformLocations();
        
    public:
        
        // IPositionShader
        virtual GLuint getViewProjectionMatrixUniform();
        virtual GLuint getModelMatrixUniform();
        
        // IAmbientShader
        virtual GLuint getAmbientColorUniform();

    };
}

#endif /* defined(__Gummy__AmbientShader__) */
