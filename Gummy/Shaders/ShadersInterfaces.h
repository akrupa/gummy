//
//  ShaderInterfaces.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef Gummy_ShaderInterfaces_h
#define Gummy_ShaderInterfaces_h

#include <OpenGL/gl3.h>
#include <iostream>

namespace Shader {
    
    class IPositionShader
    {
    public:
        virtual ~IPositionShader() {}
        virtual GLuint getViewProjectionMatrixUniform() = 0;
        virtual GLuint getModelMatrixUniform() = 0;
    };

    class IAmbientShader
    {
    public:
        virtual ~IAmbientShader() {}
        virtual GLuint getAmbientColorUniform() = 0;
    };
    
    class ILightShader
    {
    public:
        virtual ~ILightShader() {}
        virtual GLuint getLightPositionUniform() = 0;
        virtual GLuint getLightColorUniform() = 0;
    };
    
    
    
}
#endif
