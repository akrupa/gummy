//
//  Model.h
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#ifndef __Gummy__Model__
#define __Gummy__Model__

#include <stdio.h>
#include "glm.hpp"

#include "Mesh.h"
#include "Shader.h"
#include "MeshBuffer.h"

namespace Model {
    class Model {
        glm::mat4 _transformMatrix;
        Mesh::Mesh *_mesh;
        Shader::Shader *_shader;
        Buffer::MeshBuffer *_meshBuffer;
        bool useTriangle = true;
        bool m_isEnabled;
        void setupPositionUniforms(glm::mat4 viewMatrix, glm::mat4 projectionMatrix);
        void setupAmbientUniforms(glm::vec3 ambientColor);
        void setupLightUniforms(glm::vec3 lightPosition, glm::vec3 lightColor);
        
    public:
        Model(Mesh::Mesh *mesh, Shader::Shader *shader);
        
        glm::mat4 getTransformMatrix();
        void setTransformMatrix(glm::mat4 matrix);
        void draw(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 ambientColor);
        void draw(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 ambientColor, glm::vec3 lightPosition, glm::vec3 lightColor);
        void disableModel();
        void enableModel();
        bool isEnabled();
    };
}

#endif /* defined(__Gummy__Model__) */
