//
//  Model.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "Model.h"
#include "type_ptr.hpp"

Model::Model::Model(Mesh::Mesh *mesh, Shader::Shader *shader) : _mesh(mesh), _transformMatrix(glm::mat4()),_shader(shader)
{
    
    _meshBuffer = new Buffer::MeshBuffer();
    _meshBuffer->setup();
    
    _meshBuffer->fillVertexData(mesh->GetVertexData(),
                               mesh->GetVertexDataSize(),
                               0);
    
    _meshBuffer->fillIndexData(mesh->GetIndexData(), mesh->GetIndexDataSize(), 0);
    //_meshBuffer->fillIndexData((unsigned int *)mesh->triangles.data(),
    //                          mesh->triangles.size() * sizeof(Triangle),
    //                          0);
    m_isEnabled = true;
}

glm::mat4 Model::Model::getTransformMatrix()
{
    return _transformMatrix;
}

void Model::Model::setTransformMatrix(glm::mat4 matrix)
{
    _transformMatrix = matrix;
}

void Model::Model::setupPositionUniforms(glm::mat4 viewMatrix, glm::mat4 projectionMatrix) {
    Shader::IPositionShader *m_shader = dynamic_cast<Shader::IPositionShader*>(_shader);
    if (m_shader != NULL) {
        glUniformMatrix4fv(m_shader->getViewProjectionMatrixUniform(), 1, 0, glm::value_ptr(projectionMatrix*viewMatrix));
        glUniformMatrix4fv(m_shader->getModelMatrixUniform(), 1, 0, glm::value_ptr(_transformMatrix));
    }
}

void Model::Model::setupAmbientUniforms(glm::vec3 ambientColor) {
    Shader::IAmbientShader *m_shader = dynamic_cast<Shader::IAmbientShader*>(_shader);
    if (m_shader != NULL) {
        glUniform3fv(m_shader->getAmbientColorUniform(), 1, glm::value_ptr(ambientColor));
    }
}

void Model::Model::setupLightUniforms(glm::vec3 lightPosition, glm::vec3 lightColor) {
    Shader::ILightShader *m_shader = dynamic_cast<Shader::ILightShader*>(_shader);
    if (m_shader != NULL) {
        glUniform3fv(m_shader->getLightPositionUniform(), 1, glm::value_ptr(lightPosition));
        glUniform3fv(m_shader->getLightColorUniform(), 1, glm::value_ptr(lightColor));
    }
}


void Model::Model::draw(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 ambientColor) {
    if (!m_isEnabled) {
        return;
    }
    if(_mesh->isDirty) {
        _mesh->isDirty = false;
        _meshBuffer->fillVertexData(_mesh->GetVertexData(),
                                    _mesh->GetVertexDataSize(),
                                    0);
        
        _meshBuffer->fillIndexData(_mesh->GetIndexData(), _mesh->GetIndexDataSize(), 0);
    }
    glUseProgram(_shader->getProgram());
    setupPositionUniforms(viewMatrix, projectionMatrix);
    setupAmbientUniforms(ambientColor);
    
    glBindVertexArray(_meshBuffer->getVAO());
    
    //GLuint indexCount = (GLuint)(_mesh->triangles.size() * 3);
    GLuint indexCount = (GLuint)(_mesh->GetIndexDrawSize());
    glDrawElements(_mesh->GetDrawType(), indexCount, GL_UNSIGNED_INT, 0);
    
    glUseProgram(0);
    glBindVertexArray(0);
}

void Model::Model::draw(glm::mat4 viewMatrix, glm::mat4 projectionMatrix, glm::vec3 ambientColor, glm::vec3 lightPosition, glm::vec3 lightColor) {
    if (!m_isEnabled) {
        return;
    }
    if(_mesh->isDirty) {
        _mesh->isDirty = false;
        _meshBuffer->fillVertexData(_mesh->GetVertexData(),
                                    _mesh->GetVertexDataSize(),
                                    0);
        
        _meshBuffer->fillIndexData(_mesh->GetIndexData(), _mesh->GetIndexDataSize(), 0);
    }
    glUseProgram(_shader->getProgram());
    setupPositionUniforms(viewMatrix, projectionMatrix);
    setupAmbientUniforms(ambientColor);
    setupLightUniforms(lightPosition, lightColor);
    
    glBindVertexArray(_meshBuffer->getVAO());
    //GLuint indexCount = (GLuint)(_mesh->triangles.size() * 3);
    GLuint indexCount = (GLuint)(_mesh->GetIndexDrawSize());
    glDrawElements(_mesh->GetDrawType(), indexCount, GL_UNSIGNED_INT, 0);

    
    glUseProgram(0);
    glBindVertexArray(0);
}

void Model::Model::disableModel() {
    m_isEnabled = false;
}

void Model::Model::enableModel() {
    m_isEnabled = true;
}

bool Model::Model::isEnabled() {
    return m_isEnabled;
}
