//
//  Renderer.cpp
//  Gummy
//
//  Created by Adrian Krupa on 02.11.2014.
//  Copyright (c) 2014 Adrian Krupa. All rights reserved.
//

#include "Renderer.h"
#include "Mesh.h"
#include "type_ptr.hpp"
#include "matrix_transform.hpp"
#include "Model.h"
#include "CameraController.h"
#include "PlaneMesh.h"
#include "CubeLineMesh.h"
#include "Cube64PointsMesh.h"
#include "AmbientShader.h"
#include "DiffuseShader.h"
#include "SpecularShader.h"
#include "tiny_obj_loader.h"

using namespace glm;

float s = 0;

void Renderer::Renderer::setup()
{
    
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClearStencil(0);
}

void Renderer::Renderer::initScene() {
    /*
    mat4 translation = translate(mat4(), vec3(0.0f, 0.0f, 0.0f));
    float scale = 2.0f;
    
    Mesh::PlaneMesh *planeMesh = new Mesh::PlaneMesh();
    Mesh::CubeLineMesh *cubeLineMesh = new Mesh::CubeLineMesh();
    
    Shader::Shader *shader = new Shader::DiffuseShader();
    shader->setup();
    
    Model::Model * plane2 = new Model::Model(cubeLineMesh, shader);
    Model::Model * plane3 = new Model::Model(planeMesh, shader);
    plane2->setTransformMatrix(translation* glm::scale(mat4(), vec3(scale, scale, scale)));
    plane3->setTransformMatrix(translation* glm::scale(mat4(), vec3(scale, scale, scale)));

    models.push_back(plane2);
    //models.push_back(plane3);
     */
}

void Renderer::Renderer::addModel(Model::Model* model) {
    models.push_back(model);
}

void Renderer::Renderer::renderWithCameraController(Controller::CameraController *camera){
    
    glEnable(GL_MULTISAMPLE);
    glPolygonMode( GL_FRONT, GL_FILL );
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glEnable(GL_CULL_FACE);
    glPointSize(10);
    for (auto model : models) {
        model->draw(camera->getViewMatrix(), camera->getProjectionMatrix(), glm::vec3(0.5,0.5,0.5), glm::vec3(0.5,0,0.5), glm::vec3(0.7, 0.44,0.47));
    }
    s+= M_PI_4/30;
}
